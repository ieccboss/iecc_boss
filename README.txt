Pre-requisites :
################################################################################################

1. Ubuntu 16.04, 64 bit machine is required, these packages can work only on this environment.
2. NodeJs must be installed on the system.
3. Machine must be connected to internet during installation
4. mysql must be installed on this machine
5. db_connection.json file must be present in the same directory, from where iecc-asts install is run

IECC-ASTS package Installation steps:
################################################################################################
1. Download the package
2. sudo dpkg -i iecc-astsXXX(complete location if not in same directory)
3. create db_connection.json file with mysql details
4. sudo iecc-asts install
5. Wait for the installation of required packages to complete
6. If you see error for some reason, run sudo iecc-asts install command again
7. License disclaimer will appear
8. Follow the onscreen instructions.
9. IeccAsts database will be created and server will be started, the url of iecc-asts app will appear on the console
10. Hit the url to view iecc-asts app in browser
11. sudo iecc-asts stop, to stop the server
12. sudo iecc-asts start, to stop the server. Again the url of iecc-asts app will appear on the console
