var snmpjs = require('snmpjs');
var snmp = require ("net-snmp");
var bunyan = require('bunyan');
var util = require('util');
var IP;
var SESSION;

var options = {
  port: 161,
  retries: 1,
  timeout: 5000,
  transport: "udp4",
  trapPort: 162,
  version: snmp.Version1
};

var baseOid = "1.3.6.1.2.1.1"; // It should be come from configuration File.

module.exports = function(io){

var objOperations = require('./db_operations.js')(io);

function doneCb (error) {
  if (error){
    console.error (error.toString());
    if(error.toString().indexOf('RequestTimedOutError') !== -1){
      objOperations.UpdateStatusField('is_connected', 'N', IP);
    }
  }
  else{
      //objOperations.UpdateStatusField('is_connected', 'Y', IP);
  }
}

function feedCb (varbinds) {
  for (var i = 0; i < varbinds.length; i++) {
    if (snmp.isVarbindError (varbinds[i])){
      console.error (snmp.varbindError (varbinds[i]));
      return true;
    }
    else
      console.log (varbinds[i].oid + "|" + varbinds[i].value);
      var TrapObject = JSON.parse(varbinds[i].value);
      var fieldName = Object.keys(TrapObject)[0];
      var fieldValue = TrapObject[fieldName];
      objOperations.UpdateStatusField(fieldName, fieldValue, IP);
  }
}

function _SyncAllStatusParameters(ip){
  IP = ip;
  var session = snmp.createSession (ip, "public", options);
  session.on ("error", function (error) {
    console.log (error.toString());
    session.close ();
  });
  SESSION = session;
  session.walk(baseOid, feedCb, doneCb);
}

function _BeginTrapListener(){
  //var log = new bunyan({ name: 'snmpd', level: 'trace'});
  //var trapd = snmpjs.createTrapListener({log: log});
  var trapd = snmpjs.createTrapListener();

  trapd.on('trap', function(msg){
    var now = new Date();
   // console.log("Trap Received " + now);
    //console.log(snmpjs.message.serializer(msg)['pdu'].varbinds[0].string_value);
    objSNMP = JSON.parse(snmpjs.message.serializer(msg)['pdu'].varbinds[0].string_value);
    //console.log(Object.keys(objSNMP)[1], objSNMP[Object.keys(objSNMP)[1]], objSNMP['ip']);
    objOperations.UpdateStatusField(Object.keys(objSNMP)[1], objSNMP[Object.keys(objSNMP)[1]], objSNMP['ip'], snmpjs.message.serializer(msg)['pdu'].varbinds[0].oid);

  });

  trapd.bind({family: 'udp4', port: 162});

}

var objSnmp = {
  SyncAllStatusParameters : _SyncAllStatusParameters,
  BeginTrapListener : _BeginTrapListener
}
  
return objSnmp;
}
