create database ieccboss;

CREATE TABLE `config` (
  `KEY` varchar(50) NOT NULL,
  `VALUE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KEY`);

CREATE TABLE `tbl_devices_category` (
	`id` integer NOT NULL AUTO_INCREMENT,
    `cat_name` varchar(20),
    `created_on` timestamp,
	 PRIMARY KEY (`id`)
);

CREATE TABLE `tbl_devices` (
	`id` integer PRIMARY KEY AUTO_INCREMENT,
	`name`	varchar(20),
	`category`	integer,
	`port`	integer NOT NULL,
	`device_ip`	varchar(20) NOT NULL,
	`created_on`	timestamp,
	`device_location`	varchar(20),
	`software_version`	varchar(10),
	`hardware_version`	varchar(10),
	`switch_direction`	varchar(10),
	`health_status`	varchar(20),
	`is_connected`	varchar(5),
	`latch_out_status`	varchar(20),
	`aux_sensors_value`	numeric,
	`aux_sensors_threshold`	numeric,
	`motor_current_value`	numeric,
	`motor_current_threshold`	numeric,
	`motor_throw_time_value`	numeric,
	`motor_throw_time_threshold`	numeric,
	`voltage_red_threshold`	numeric,
	`voltage_yellow_threshold`	numeric,
	`voltage_value`	numeric,
	`repo_loc`	TEXT,
	FOREIGN KEY(`category`) REFERENCES tbl_devices_category ( id )
);

CREATE TABLE `DEVICE_INST_DATA` (
  `device_id` int(11) NOT NULL,
  `latch_out_config_setting` varchar(9) DEFAULT NULL,
  `position_setting` varchar(9) DEFAULT NULL,
  `ipd_mode` varchar(16) DEFAULT NULL,
  `part_number` varchar(16) DEFAULT NULL,
  `serial_number` varchar(16) DEFAULT NULL,
  `system_software_version` varchar(12) DEFAULT NULL,
  `comm_processor_version` varchar(12) DEFAULT NULL,
  `vital_fpga_altera_version` varchar(12) DEFAULT NULL,
  `vital_fpga_xilinx_version` varchar(12) DEFAULT NULL,
  `system_hardware_version` varchar(12) DEFAULT NULL,
  `switch_mac_part_num` varchar(16) DEFAULT NULL,
  `switch_mac_serial_num` varchar(16) DEFAULT NULL,
  `switch_mac_type` varchar(6) DEFAULT NULL,
  `switch_mac_gear_ratio` varchar(6) DEFAULT NULL,
  `switch_mac_direction_installation` varchar(6) DEFAULT NULL,
  `switch_mac_motor_controller` varchar(6) DEFAULT NULL,
  `switch_mac_motor_v` varchar(6) DEFAULT NULL,
  `mile_post` varchar(6) DEFAULT NULL,
  `chaining_mode` varchar(9) DEFAULT NULL,
  `nominal_position_gps` varchar(50) DEFAULT NULL,
  `manufacturing_date` varchar(20) DEFAULT NULL,
  `switch_mac_manufacturing_shipping_date` varchar(25) DEFAULT NULL,
  `switch_mac_installation_date` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`device_id`),
  CONSTRAINT `DEVICE_INST_DATA_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `tbl_devices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `MOTOR_CURRENT_FILE` (
  `FILE_NAME` text,
  `TIME_STAMP` datetime DEFAULT CURRENT_TIMESTAMP,
  `DEVICE_ID` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE USER_ACCOUNT (username varchar(100) primary key, password varchar(100));

DELIMITER //
CREATE PROCEDURE CreateLogTables(IN id varchar(10))
 BEGIN
 SET @eventTableSQL = CONCAT('CREATE TABLE ', CONCAT(id,'_events'), '(id INTEGER PRIMARY KEY AUTO_INCREMENT, source smallint not null, primaryid INT, secondaryid INT, level smallint not null, errtype smallint, time TEXT, description TEXT);');
 SET @maintenanceTableSQL = CONCAT('CREATE TABLE ', CONCAT(id,'_maintenance'), '(SWITCH_MACHINE_MAINTENANCE_NUM INTEGER PRIMARY KEY AUTO_INCREMENT, LOG_DATE	TEXT, INSPECTION_START_TIME	TEXT, INSPECTION_END_TIME TEXT, INSPECTION_DURATION	TEXT, INSPECTION_DESCRIPTION TEXT, INSPECTION_PERSONNEL TEXT, TIME_SINCE_LAST_INSPECTION TEXT);');
 SET @swopTableSQL = CONCAT('CREATE TABLE ', CONCAT(id,'_swop'), '(id integer primary key auto_increment, start text, end text, opertime int8, mcamps real, temp smallint, throwcount integer, time text);');
 SET @snmpTableSQL = CONCAT('CREATE TABLE ', CONCAT(id,'_snmp'), '(id integer primary key auto_increment, text_value text, numeric_id text, time text, criticality text);');
 PREPARE stmt from @eventTableSQL;
 EXECUTE stmt;
 PREPARE stmt from @maintenanceTableSQL;
 EXECUTE stmt;
 PREPARE stmt from @swopTableSQL;
 EXECUTE stmt;
 PREPARE stmt from @snmpTableSQL;
 EXECUTE stmt;
 END //
DELIMITER ;

ALTER TABLE tbl_devices
ADD initial_poll varchar(10);

ALTER TABLE tbl_devices
ADD initial_poll_processing varchar(10);


DELIMITER $$
CREATE TRIGGER DEVICE_INST_DATA_TRIGGER
AFTER INSERT ON `tbl_devices` FOR EACH ROW
begin
		INSERT INTO DEVICE_INST_DATA (device_id) values (NEW.id);
END;
$$
DELIMITER ;


insert into config values ("VOLTAGE_RED_THRESHOLD","200"),("VOLTAGE_YELLOW_THRESHOLD","180"),("VOLTAGE_GREEN_THRESHOLD","60"),("BOSS_IP_ADDRESS","192.168.1.5"),("SNMP_RECEIVER_IP_ADDRESS","192.168.1.5"),("EVENT_LOG_FLAG","true"),("SWOP_LOG_FLAG","true"),("MAINTENANCE_LOG_FLAG","true"),("MOTOR_CURRENT_LOG_FLAG","true"),("LOG_POL_RATE","*/5 * * * *"),("INS_DATA_POL_RATE","*/5 * * * *"),("SNMP_LOG_FLAG","false"),("SNMP_LOG_MAX_SIZE","5"),("MOTOR_CURR_LOG_MAX_SIZE","5");
insert into `tbl_devices_category`(`cat_name`,`created_on`) values('Unassigned',current_timestamp);

