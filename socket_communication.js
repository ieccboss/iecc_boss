module.exports = function(io){
  var logs = require('./routes/logs.js');
  var exec = require('child_process').exec;
  var path = require('path');
  var getSize = require('get-folder-size');
  var objOperations = require('./db_operations.js')(io);
  var snmpHandler = require('./snmp_trap_handler')(io);
  var fs = require('fs');
  var motor_current_file_base_location =  path.join(__dirname,'.','repo/');
  var njds = require('nodejs-disks');

  io.sockets.on('connection', function (socket) {

    function DecideColor(rawDeviceList, categoryList, emit_tag, callback){
      var deviceList=[];
      for (var i=0 ; i < rawDeviceList.length ; i++) {
        var objDeviceList = {};
        objDeviceList.dev_id = rawDeviceList[i].dev_id;
        objDeviceList.dev_name = rawDeviceList[i].dev_name;
        objDeviceList.cat_id = rawDeviceList[i].cat_id;
        objDeviceList.connection_status = rawDeviceList[i].is_connected == 'Y' ? 'green' : 'gray';
        objDeviceList.latch_out_status = rawDeviceList[i].latch_out_status == 'LO' ? 'red' : 'green';
        objDeviceList.health_status =  rawDeviceList[i].health_status ==  'G' ? 'green' : 'red';
        objDeviceList.aux_sensors_status = parseInt(rawDeviceList[i].aux_sensors_value) > parseInt(rawDeviceList[i].aux_sensors_threshold) ? 'yellow' : 'green';
        objDeviceList.motor_current_status = parseInt(rawDeviceList[i].motor_current_value) > parseInt(rawDeviceList[i].motor_current_threshold) ? 'yellow' : 'green';
        objDeviceList.motor_throw_time_status = parseInt(rawDeviceList[i].voltage_value) > parseInt(rawDeviceList[i].voltage_red_threshold) ? 'red' : (parseInt(rawDeviceList[i].voltage_value) > parseInt(rawDeviceList[i].voltage_yellow_threshold) ? 'yellow' : 'green');
        deviceList.push(objDeviceList);
      }
      callback(deviceList, categoryList, emit_tag);
    }

    function BuildObjectForTree(data, emit_tag){
      DecideColor(data.deviceList, data.categoryList, emit_tag, function(deviceList, categoryList, emit_tag){
        var treeData = categoryList;
        treeData.map(v => v.node = []);
        for (var device in deviceList){
          treeData.filter(x => x.cat_id === deviceList[device].cat_id).map(x => x.node.push(deviceList[device]));
        }
        socket.emit(emit_tag, { status: '0',
          data : JSON.stringify(treeData)
        });
      })
    }

    socket.on('get-tree-data', function(){
      objOperations.GetTreeData(function(error, data){
        if(error)
          console.log(error);
        else
          BuildObjectForTree(data, 'tree_data');
      });
    });


    socket.on('update-categories', function(parems){
      objOperations.UpdateDeviceCategory(parems.deviceID, parems.categoryID, function(error, data){
        if(error)
          console.log(error);
        else
          BuildObjectForTree(data, 'tree_data');
      });
    });

    socket.on('add-new-devices', function(deviceList, callback){
      objOperations.AddNewDevices(deviceList, function(statusObj){
        for (var i = 0; i < deviceList.length; i++) {
          var IP = deviceList[i].ip;
          snmpHandler.SyncAllStatusParameters(IP);
        }
        console.log(statusObj);
        callback(statusObj);
      })
    });

    socket.on('update-device-info', function(devicedata, callback){
      objOperations.UpdateDeviceInfo(devicedata, function(statusObj){
        callback(statusObj);
      })
    });

    socket.on('get-device-info', function(deviceID, callback){
      objOperations.GetDeviceInfo(deviceID, function(error, data){
        if(error){
          callback({status:"error", data : ""});
        }
        callback({status:"ok", data : data});
      });
    });

    socket.on('save-category', function(categoryName, callback){
      objOperations.SaveCategory(categoryName, function(objStatus){
        callback(objStatus);
      });
    });

    socket.on('get_swop_logs', function(parems){
      objOperations.GetSwopLogs(parems.recordsPerPage, parems.pageNumber, parems.tableName, function(data){
        socket.emit('swop_log', data);
      });
    });

    socket.on('get_snmp_logs', function(parems){
      objOperations.GetSnmpLogs(parems.recordsPerPage, parems.pageNumber, parems.tableName, function(data){
        socket.emit('snmp_log', data);
      });
    });

    socket.on('getEventLog', function(param,pageSize,pageNum){
      logs.processEventLog(param,pageSize,pageNum, function(logs,tot){
        socket.emit('eventLog', logs,tot);
      });
    });

    socket.on('getMaintenanceLog', function(param,pageSize,pageNum){
      logs.processMaintenanceLog(param,pageSize,pageNum,function(logs,tot){
        socket.emit('maintenanceLog', logs,tot);
      });
    });

    socket.on('updBossConf', function(param){
      objOperations.GetConfigurableIP(function(data){
        objOperations.updateBossConfig(param,function(err, ch){
          console.log('updBossConf--claback='+ch);
          logs.getCronsConfig();
          var CurrentIP = data[0].VALUE;
          if(data[0].VALUE != param.BOSS_IP_ADDRESS){
            _BindConfigurableIPAndDeletePrevious(CurrentIP, param.BOSS_IP_ADDRESS);
          }
          socket.emit('updBossConfUpdated', ch);
        });
      });
    });

    function _BindConfigurableIPAndDeletePrevious(IPAddressToBeRemove, NewIP){
      var IP = NewIP;
      var Networkinterfaces = require('os').networkInterfaces();
      var NetwoekInterfaceList = Object.keys(Networkinterfaces);
      NetwoekInterfaceList.splice(NetwoekInterfaceList.indexOf('lo'), 1);
      var counter = 0;
      for (var NI in NetwoekInterfaceList) {
        command1 = "sudo ip address del "+ IPAddressToBeRemove +"/24 dev " + NetwoekInterfaceList[NI];
        exec(command1, function (error1, stdout1, stderr1) {
          counter++;
          if(counter == NetwoekInterfaceList.length){
            command = "sudo ip address add "+ IP +"/24 dev " + "wlp2s0";
            exec(command, function (error, stdout, stderr) {
              if(error){
                console.log(error)
              }
              else{
                console.log("ASTS is listening on " + IP +":80");
              }
            });
          }
        });
      }
    }

    socket.on('getInstData', function(param){
      console.log(param);
      logs.processInstData(param,function(logs){
        socket.emit('getInstData', logs);
      });
    });

    socket.on('getLogsSize', function(){
      objOperations.GetLogsSize(function(threshold){
        var repo = path.join(__dirname,'.','repo/');
        getSize(repo, function(err, size) {
          if (err) { throw err; }
          //console.log(size + ' bytes');
          //console.log((size / 1024 / 1024).toFixed(2) + ' Mb');
          var objLog = { logsize : (size / 1024 / 1024).toFixed(2), thershold : threshold, log_name : 'motor_current' };
          socket.emit('logStorageInfo', objLog);
        });
      });
    });

    socket.on('get-disk-space', function(){
      njds.drives(
        function (err, drives) {
          njds.drivesDetail(
            drives,
            function (err, data) {
              for(var i = 0; i<data.length; i++)
              {
                /* Get drive mount point */
                if(data[i].mountpoint == "/"){
                  socket.emit('disk-space', { totaldiskSpace : data[i].total.replace(' MB','').replace(' GB',''), usedSpace : data[i].used.replace(' MB','').replace(' GB',''), threshold : 95 });
                }

              }
            }
            );
        }
        )
    });

    socket.on('get-mix-and-max-date', function(logName){
      objOperations.GetMinAndMaxDate(logName, function(data){
        socket.emit('min-max-date', data);
      });
    });

    socket.on('delete-logs', function(parms, callback){
      if(parms.logName == "motor_current"){
        objOperations.GetMotorCurrentFileLocationForDelete(parms, function(objData){
          console.log(objData);
          var counter = 0;
          for(var i=0; i < objData.length; i++){
            fs.unlink(motor_current_file_base_location + objData[i].File_Location, (err) => {
              counter++;
              if (err) throw err;
              else{
                if (counter == objData.length)
                  objOperations.DeleteMotorCurrentFiles(parms, function(status){
                    callback(status);
                  });
              }
            });
          }
        })
      }
      else{
        objOperations.DeleteLogs(parms, function(status){
          callback(status);
        })
      }
    });


    socket.on('get-Conf-data', function(callback){
      objOperations.GetCongigurationData(function(err, data){
        if(err){
          console.log(err)
        }
        else{
          callback(data);
        }
      })
    });

    socket.on('delete-category', function(catergoryID, callback){
      objOperations.ValidateAndDeleteCategory(catergoryID, function(data){
        callback(data);
      });
    });

    var deleteFolderRecursive = function(repoLocation) {
    if( fs.existsSync(repoLocation) ) {
      fs.readdirSync(repoLocation).forEach(function(file,index){
        var curPath = repoLocation + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(repoLocation);
    }
  };

    socket.on('delete-device', function(deviceID, callback){
      objOperations.DeleteDevice(deviceID, function(data){
        if(data.status == "ok")
        {
          var repoLocation = path.join(motor_current_file_base_location, deviceID);
          deleteFolderRecursive(repoLocation);
          callback(data);
        }
        else
          callback(data);
      });
    });

  });


function _BindConfigurableIP(FixedIP){
  objOperations.GetConfigurableIP(function(data){
    var IP = data[0].VALUE;
    var Networkinterfaces = require('os').networkInterfaces();
    var NetwoekInterfaceList = Object.keys(Networkinterfaces);
    NetwoekInterfaceList.splice(NetwoekInterfaceList.indexOf('lo'), 1);
    var counter = 0;
    if( IP != FixedIP){
      for (var NI in NetwoekInterfaceList) {
        command1 = "sudo ip address del "+ IP +"/24 dev " + NetwoekInterfaceList[NI];
        exec(command1, function (error1, stdout1, stderr1) {
          counter++;
          if(counter == NetwoekInterfaceList.length){
            command = "sudo ip address add "+ IP +"/24 dev " + "wlp2s0";
            exec(command, function (error, stdout, stderr) {
              if(error){
                console.log(error)
              }
              else{
                console.log("IECC ASTS is running at " + IP +":80");
              }
            });
          }
        });
      }
    }
  });
}

var objSocket_Communication = {
  BindConfigurableIP : _BindConfigurableIP
}

return objSocket_Communication;

}
