var express = require('express')
, app = express()
, admin = require('./routes/admin')
, logs = require('./routes/logs')
//, sqlite3 = require('./web_modules/node_modules/sqlite3')
, bodyParser = require('body-parser')
, session = require('express-session')
, exec = require('child_process').exec
, path = require('path')
, xml2js = require('xml2js')
, ip = require("ip");

var logger = require('console-stamp')(console, '[HH:MM:ss.l]');

app.use(session({ secret: 'ieccbossprivate', resave: true, saveUninitialized: false, cookie: { maxAge: 60000 },rolling: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/*express.logger.format('mydate', function() {
    var df = require('console-stamp/node_modules/dateformat');
    return df(new Date(), 'HH:MM:ss.l');
})
app.use(express.logger('[:mydate] :method :url :status :res[content-length] - :remote-addr - :response-time ms'));;*/

const socketIO = require('socket.io');
app.set('port', process.env.PORT || 80);
app.set('ip', process.env.IP || ip.address());


var server = app.listen(app.get('port'), function () {
  console.log("IECC ASTS is running at "+ app.get('ip')+":"+app.get('port'))
  var command = "avahi-resolve --address " + app.get('ip');
  exec(command, function (error, stdout, stderr) {
    if(error)
      console.log(error);
    else{
      console.log("Type " + stdout.replace(app.get('ip'), '').trim() + " in your web browser to open ASTS server.");
    }
  });
})



const io = socketIO(server);

var objOperations = require('./db_operations.js')(io);

var categoryList;
function setCategoryList(callback){
  objOperations.GetCategoryList( function(err, data){
    if(err)
      categoryList = "";
    else
      categoryList = data;
    callback();
  });
}


//app.use('/public', express.static('public'));
//snmpHandler.SyncAllStatusParameters('172.18.42.1');
var snmpHandler = require('./snmp_trap_handler')(io);
snmpHandler.BeginTrapListener();
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'repo')));




//app.set('db', new sqlite3.Database('./database/ieccboss'));
app.set('views', __dirname + '/public/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.set('view options', { layout: false });


app.get('/', function (req, res) {
 res.render('index.ejs',{watcher: req.session.user,maxTime: parseInt(req.session.cookie.maxAge/1000)});
})

app.get('/deviceEntry', function (req, res) {
 // console.log(categoryList);
  setCategoryList(function(){
   res.render('deviceEntry.ejs', {
    categoryList : categoryList,
    watcher: req.session.user,maxTime: parseInt(req.session.cookie.maxAge/1000)
  });
 });
})
//app.post('/updDeviceInfo', admin.updDeviceInfo);

app.get('/deviceConfiguration', function (req, res) {
	//console.log('req.session'+req.session);
	if(typeof req.session == 'undefined' || req.session == null || typeof req.session.user == 'undefined' || req.session.user == null){
    res.render('index.ejs',{watcher: req.session.user,maxTime: parseInt(req.session.cookie.maxAge/1000)});
  }else{
   res.render('deviceConfiguration.ejs',{watcher: req.session.user,maxTime: parseInt(req.session.cookie.maxAge/1000), ip : app.get('ip')});
 }
})

app.post('/adminAuth', admin.authenticate);
app.post('/checkSession', admin.checkSession);
app.post('/endSession', admin.endSession);
app.post('/renewSession', admin.renewSession);

app.post('/latestLog', logs.getLatestMotorCurrentFile);
app.post('/specificLog', logs.getSpecificLog);
app.post('/currentFiles', logs.getCurrentFiles);

var socket_events = require('./socket_communication')(io);
console.log(socket_events);
socket_events.BindConfigurableIP(app.get('ip'));
logs.getCronsConfig();

//logs.resetLogsCrons('*/1 * * * *',false,true,true,true,'');//logfreq,crrFlag,swopFlag,maintFlag,eventFlag,instFreq
