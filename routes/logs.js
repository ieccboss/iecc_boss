var parseString = require('xml2js').parseString;
var mysql = require('mysql');
var config_connection_obj = require('../db_connection.json');
var path = require('path');
var repo = path.join(__dirname,'..','repo/');
var connection = mysql.createConnection(config_connection_obj);
var connectEv = mysql.createConnection(config_connection_obj);
var connectMaint = mysql.createConnection(config_connection_obj);
var connectSwop = mysql.createConnection(config_connection_obj);
var connectInst = mysql.createConnection(config_connection_obj);
var connectCurr = mysql.createConnection(config_connection_obj);

connection.connect();
connectEv.connect();
connectMaint.connect();
connectSwop.connect();
connectInst.connect();
connectCurr.connect();

exports.getLatestMotorCurrentFile = function(req, res){
  var responseJSON = {};
  var devId = req.body.devId;
  function getResFromDb(callback) {
    connection.query("SELECT FILE_NAME FROM MOTOR_CURRENT_FILE where device_id='"+devId+"' and TIME_STAMP = (SELECT max(TIME_STAMP) FROM MOTOR_CURRENT_FILE where device_id='"+devId+"')", function(err, result) {
      if(err)
      callback(err, null);
      else if( !result[0] || result[0].FILE_NAME == null)
      callback('fail','no record');
      else
      callback('pass', result);
    }.bind(this));
  }
  function processResult(err, result) {
    if(err=='pass'){
      responseJSON.status='200';
      responseJSON.file=result[0].FILE_NAME;
    } else if(err=='fail'){
      responseJSON.status='404';
    } else {
      responseJSON.status='500';
    }
    res.send(responseJSON);
  }
  getResFromDb(processResult);

};


exports.getCurrentFiles = function(req, res){
  var responseJSON = {};
  var device = req.body.devId;
  function getResFromDb(callback) {
    connection.query('select FILE_NAME from MOTOR_CURRENT_FILE where DEVICE_ID =?',[device], function(err, results) {
      if(err)
      callback(err, null);
      else if( !results )
      callback('fail','no record');
      else
      callback('pass', results);
    }.bind(this));
  }
  function processResult(err, currentFiles) {
    if(err=='pass'){
      responseJSON.status='200';
      responseJSON.currentFiles=currentFiles;
    } else if(err=='fail'){
      responseJSON.status='404';
    } else {
      responseJSON.status='500';
    }
    res.send(responseJSON);
  }
  getResFromDb(processResult);
};

exports.getSpecificLog = function(req, res){
  var responseJSON = {};
  var device = req.body.device;
  var date = req.body.date;
  function getResFromDb(callback) {
    connection.query('select FILE_NAME from MOTOR_CURRENT_FILE where date(TIME_STAMP) =? and DEVICE_ID =? and FILE_NAME !=?',[date, device,req.body.latestGraphFile], function(err, results) {
      if(err)
      callback(err, null);
      else if( !results )
      callback('fail','no record');
      else
      callback('pass', results);
    }.bind(this));
  }
  function processResult(err, currentFiles) {
    if(err=='pass'){
      responseJSON.status='200';
      responseJSON.currentFiles=currentFiles;
    } else if(err=='fail'){
      responseJSON.status='404';
    } else {
      responseJSON.status='500';
    }
    res.send(responseJSON);
  }
  getResFromDb(processResult);

};

var cron = require('node-cron');
var fs = require('fs');
var Client = require('node-rest-client').Client;
var eventsApi = '/api/getEventLogs/';
var maintenanceApi = '/api/getMaintenanceLogs/';
var swopApi = '/api/getSwopLogs/';
var motorCurrentApi = '/api/getMotorCurrentFile/';
var instApi = 'getInstData/';

var eventsLogFile = 'events.db';
var maintenanceLogFile = 'maintenance.db';
var swopLogFile = 'swop.db';
var motorCurrentDir = 'motorCurrent';
var eventLogCron;
var motorCurrentCron;
var swopLogCron;
var maintenanceLogCron;


var fetch = require('node-fetch');

exports.resetLogsCrons = function(logfreq,crrFlag,swopFlag,maintFlag,eventFlag,instFreq){
  console.log('Polling started.');
  if(eventLogCron && motorCurrentCron && swopLogCron && maintenanceLogCron){
    motorCurrentCron.stop();
    swopLogCron.stop();
    maintenanceLogCron.stop();
    eventLogCron.stop();
  }
  if(logfreq){
    eventLogCron = cron.schedule(logfreq, function(){
      getDeviceId('eventsApi',eventsLogFile,processResult);
    }, false);

    motorCurrentCron = cron.schedule(logfreq, function(){
      getDeviceId('motorCurrentApi',motorCurrentDir,processResult);
    }, false);

    swopLogCron = cron.schedule(logfreq, function(){
      getDeviceId('swopApi',swopLogFile,processResult);
    }, false);

    maintenanceLogCron = cron.schedule(logfreq, function(){
      getDeviceId('maintenanceApi',maintenanceLogFile,processResult);
    }, false);

    //currentCronFreq = logfreq;
    //swopLogCronFreq = logfreq;
    //maintenanceLogCronFreq = logfreq;
    //eventLogCronFreq = logfreq;
    //instDataCronFreq = instFreq;
    console.log(crrFlag);
    console.log(swopFlag);
    console.log(maintFlag);
    console.log(eventFlag);
    if(crrFlag == 'true'){motorCurrentCron.stop();motorCurrentCron.start();}
    if(swopFlag == 'true'){swopLogCron.stop();swopLogCron.start();}
    if(maintFlag == 'true'){maintenanceLogCron.stop();maintenanceLogCron.start();}
    if(eventFlag == 'true'){eventLogCron.stop();eventLogCron.start();}

  }
  var instDataCron;

  if(instFreq && instFreq!='' && instFreq!=null){
    instDataCron = cron.schedule(instFreq, function(){
      getDeviceId('instApi','db',processResult);
    }, false);
    instDataCron.stop(); instDataCron.start();
  }
}

exports.getCronsConfig = function(callback){
  connection.query("select `KEY`, `VALUE` from config where `KEY` in ('LOG_POL_RATE','MOTOR_CURRENT_LOG_FLAG','SWOP_LOG_FLAG','MAINTENANCE_LOG_FLAG','EVENT_LOG_FLAG','INS_DATA_POL_RATE') order by `KEY`;",
  function(err, rows) {
    if(err);
    else {
      if(rows[0]){
        startCrons(rows);
      }
    }
  });
}

function startCrons(rows){
  module.exports.resetLogsCrons(rows[2].VALUE,rows[4].VALUE,rows[5].VALUE,rows[3].VALUE,rows[0].VALUE,rows[1].VALUE);//logfreq,crrFlag,swopFlag,maintFlag,eventFlag,instFreq
}

function getDeviceId(api,logFile,callback) {
  var sql = "select id, device_ip, port, initial_poll, initial_poll_processing from tbl_devices";
  connection.query(sql, function (err, rows) {
    if (err) callback(err, null);
    else if( !rows )
    callback(api,logFile,'fail','no record');
    else{
      callback(api,logFile,'pass', rows);
    }
  });
}

function processResult(api,logFile,err, rows) {
  var url=null;
  var location=null;
  var deviceId=null;
  var is_initial_poll='0000';
  var is_initial_poll_processing='00000';
  for(var i=0; i<rows.length;i++) {
    deviceId=rows[i].id;
    is_initial_poll = rows[i].initial_poll;
    is_initial_poll_processing = rows[i].initial_poll_processing;
    if(api == 'instApi'){
      url = "http://"+rows[i].device_ip+":"+rows[i].port;
      updateInstData(url,deviceId,saveInstData);
    }else if(api == 'eventsApi'){
      if(is_initial_poll_processing.charAt(2) != '1'){
        url = "http://"+rows[i].device_ip+":"+rows[i].port+eventsApi;
        saveLogFile(deviceId,url,logFile, is_initial_poll, insertEventLog);
      }
      //     saveLogFile(deviceId,url,logFile,eventsCronIO); // with io
    }else if(api == 'maintenanceApi'){
      if(is_initial_poll_processing.charAt(1) != '1'){
        url = "http://"+rows[i].device_ip+":"+rows[i].port+maintenanceApi;
        saveMaintenanceLogFile(deviceId,url,logFile, is_initial_poll, insertMaintenanceLog);
      }
      // saveMaintenanceLogFile(deviceId,url,logFile,maintainenceCronIO); // with io
    }else if(api == 'swopApi'){
      if(is_initial_poll_processing.charAt(0) != '1'){
        url = "http://"+rows[i].device_ip+":"+rows[i].port+swopApi;
        saveSwopLogFile(deviceId,url,logFile, is_initial_poll, insertSwopLog);
      }
      // saveSwopLogFile(deviceId,url,logFile,swopCronIO);// with io
    }else if(api == 'motorCurrentApi'){
      url = "http://"+rows[i].device_ip+":"+rows[i].port+motorCurrentApi;
      saveCurrentFile(deviceId,url);
    }

  }
}
function saveCurrentFile(deviceId,url){
  fetch(url).then(function(res) {
    if(res.status==200){
      var dest = fs.createWriteStream(repo+deviceId+'/'+res.headers.get('file'));
      res.body.pipe(dest);
    }
    return res;
  }).then(function(res) {
    var file=res.headers.get('file')
    if(file){
      var sql = "INSERT INTO MOTOR_CURRENT_FILE(FILE_NAME,TIME_STAMP,DEVICE_ID) values ('"+file.substring(0, file.indexOf('.'))+"', CURRENT_TIMESTAMP, '"+deviceId+"')";
      connectCurr.query(sql, function (err, result) {
        if (err);
      });
    }
  }).catch(function(err) {
  });
}

//var socket;
function updateInstData(url,deviceId,callback){
  var socket = require('socket.io-client')(url);
  socket.on('connect', function(socket){
  });
  callback(url,deviceId,socket);
}

function saveInstData(url,deviceId,socket){
  var selectedIp = url.substring(url.lastIndexOf('/') + 1,url.lastIndexOf(':'));
  socket.emit('getInstData',selectedIp);
  socket.on('getInstData',function(message){
    parseString(message, function (err, result) {
      var query = "update DEVICE_INST_DATA set latch_out_config_setting="+result.LongTermData.iecc_node_id+", position_setting='"+result.LongTermData.iecc_position_setting+"' " +
      ", ipd_mode ='"+result.LongTermData.ipd_mode+"', part_number='"+result.LongTermData.iecc_part_number+"', serial_number='"+result.LongTermData.iecc_serial_number+"'" +
      ", system_software_version='"+result.LongTermData.system_software_version+"' , comm_processor_version='"+result.LongTermData.comm_processor_version+"', " +
      "vital_fpga_altera_version='"+result.LongTermData.vital_fpga_altera_version+"', vital_fpga_xilinx_version='"+result.LongTermData.vital_fpga_xilinx_version+"'," +
      " system_hardware_version='"+result.LongTermData.system_hardware_version+"', switch_mac_part_num='"+result.LongTermData.switch_machine_part_number+"' " +
      ", switch_mac_serial_num='"+result.LongTermData.switch_machine_serial_number+"', switch_mac_type='"+result.LongTermData.switch_machine_type+"', switch_mac_gear_ratio='"+result.LongTermData.switch_machine_gear_ratio+"' ," +
      " switch_mac_direction_installation='"+result.LongTermData.switch_machine_direction_installation+"', switch_mac_motor_controller='"+result.LongTermData.switch_machine_motor_controller+"'," +
      " switch_mac_motor_v='"+result.LongTermData.switch_machine_motor_voltage+"', mile_post='"+result.LongTermData.mile_post+"', chaining_mode='"+result.LongTermData.chaining_mode+"', nominal_position_gps='"+result.LongTermData.nominal_position_gps+"'," +
      " manufacturing_date='"+result.LongTermData.iecc_manufacturing_date+"', switch_mac_manufacturing_shipping_date='"+result.LongTermData.iecc_switch_machine_manufacturing_shipping_date+"'," +
      " switch_mac_installation_date ='"+result.LongTermData.iecc_switch_machine_installation_date+"' where device_id = "+deviceId;
      connectInst.query(query, deviceId, function (err, result) {
        if (err);
      });
    });
  });
}

function saveLogFile(deviceId,url,logFile, is_initial_poll,callback){
  var sql = "select COALESCE(max(id),0) as id from "+deviceId+"_events";
  connectEv.query(sql, function (err, rows) {
    if (err);
    else{
      callback(deviceId,url,rows[0].id, is_initial_poll);
    }
  });
}

function insertEventLog(deviceId,url,result,is_initial_poll){
  if(is_initial_poll.charAt(2) == "1"){
    var numberOfRecords = 50000;
    var query = "update tbl_devices t1 "
    +"inner join tbl_devices t2 "
    +"on t1.id = t2.id "
    +"set t1.initial_poll_processing = CONCAT(SUBSTRING(t2.initial_poll_processing,1,2),'1',SUBSTRING(t2.initial_poll_processing,4,4)) "
    +"where t1.id = ?";
    connectEv.query(query, deviceId, function (err, result) {
      if (err);
    });
  }
  else
  var numberOfRecords = 10000;
  fetch(url+result+ "/" + numberOfRecords)
  .catch(function(err) {
    if(is_initial_poll.charAt(0) == "1"){
      query = "update tbl_devices t1 "
      +"inner join tbl_devices t2 "
      +"on t1.id = t2.id "
      +"set t1.initial_poll_processing = CONCAT(SUBSTRING(t2.initial_poll_processing,1,2),'0',SUBSTRING(t2.initial_poll_processing,4,4)) "
      +"where t1.id = ?";
      connectEv.query(query ,deviceId, function (err, result) {
        if (err);
      });
    }
  }).then(function(res) {
    return res.json();
  }).then(function(json) {
    if(json.length >0){
      if(is_initial_poll.charAt(2) == "1" && json.length < 50000){
        query = "update tbl_devices t1 "
        +"inner join tbl_devices t2 "
        +"on t1.id = t2.id "
        +"set t1.initial_poll = CONCAT(SUBSTRING(t2.initial_poll,1,2),'0',SUBSTRING(t2.initial_poll,4,4)), "
        +"t1.initial_poll_processing = CONCAT(SUBSTRING(t2.initial_poll_processing,1,2),'0',SUBSTRING(t2.initial_poll_processing,4,4)) "
        +"where t1.id = ?";
        connectEv.query(query ,deviceId, function (err, result) {
          if (err);
        });
      }
      var values='';
      var x='';
      for(var i=0;i<json.length;i++){
        if(i==(json.length-1)){
          x='';
        }else{
          x=',';
        }
        values = values+"("+json[i].id+","+json[i].source+","+json[i].primaryid+","+json[i].secondaryid+","+json[i].level+","+json[i].errtype+",'"+
        json[i].time+"','"+json[i].description+"')"+x;
      }
      var params = {tableName : deviceId + '_events', orderByClause : 'time', numberOfRowsToBeDelete : json.length};
      _ManageLogSpace(params, function(){
        var sql = "INSERT INTO "+deviceId+"_events(id,source, primaryid, secondaryid, level, errtype, time, description) values " +values;
        connectEv.query(sql, function (err, result) {
          if (err);
          if(is_initial_poll.charAt(2) == "1" && json.length == 50000){
            insertEventLog(deviceId, url, json[json.length -1].id, is_initial_poll);
          }
        });
      })
    }
  }).catch(function(err) {
  });
}

function saveMaintenanceLogFile(deviceId,url,logFile, is_initial_poll, callback){
  var sql = "select COALESCE(max(SWITCH_MACHINE_MAINTENANCE_NUM),0) as id from "+deviceId+"_maintenance";
  connectMaint.query(sql, function (err, rows) {
    if (err);
    else{
      callback(deviceId,url,rows[0].id, is_initial_poll);
    }
  });
}

function insertMaintenanceLog(deviceId,url,result,is_initial_poll){
  if(is_initial_poll.charAt(1) == "1"){
    var numberOfRecords = 50000;
    var query = "update tbl_devices t1 "
    +"inner join tbl_devices t2 "
    +"on t1.id = t2.id "
    +"set t1.initial_poll_processing = CONCAT(SUBSTRING(t2.initial_poll_processing,1,1),'1',SUBSTRING(t2.initial_poll_processing,3,4)) "
    +"where t1.id = ?";
    connectMaint.query(query, deviceId, function (err, result) {
      if (err);
    });
  }
  else
  var numberOfRecords = 10000;
  fetch(url+result+ "/" + numberOfRecords)
  .catch(function(err) {
    if(is_initial_poll.charAt(0) == "1"){
      query = "update tbl_devices t1 "
      +"inner join tbl_devices t2 "
      +"on t1.id = t2.id "
      +"set t1.initial_poll_processing = CONCAT(SUBSTRING(t2.initial_poll_processing,1,1),'0',SUBSTRING(t2.initial_poll_processing,3,4)) "
      +"where t1.id = ?";
      connectMaint.query(query ,deviceId, function (err, result) {
        if (err);
      });
    }
  }).then(function(res) {
    return res.json();
  }).then(function(json) {
    if(json.length >0){
      if(is_initial_poll.charAt(1) == "1" && json.length < 50000){
        query = "update tbl_devices t1 "
        +"inner join tbl_devices t2 "
        +"on t1.id = t2.id "
        +"set t1.initial_poll = CONCAT(SUBSTRING(t2.initial_poll,1,1),'0',SUBSTRING(t2.initial_poll,3,4)), "
        +"t1.initial_poll_processing = CONCAT(SUBSTRING(t2.initial_poll_processing,1,1),'0',SUBSTRING(t2.initial_poll_processing,3,4)) "
        +"where t1.id = ?";
        connectMaint.query(query ,deviceId, function (err, result) {
          if (err);
        });
      }
      var values='';
      var x='';
      for(var i=0;i<json.length;i++){
        if(i==(json.length-1)){
          x='';
        }else{
          x=',';
        }
        values = values+"('"+json[i].SWITCH_MACHINE_MAINTENANCE_NUM+"','"+json[i].LOG_DATE+"','"+json[i].INSPECTION_START_TIME+"','"+json[i].INSPECTION_END_TIME+"','"
        +json[i].INSPECTION_DURATION+"','"+json[i].INSPECTION_DESCRIPTION+"','"+
        json[i].INSPECTION_PERSONNEL+"','"+json[i].TIME_SINCE_LAST_INSPECTION+"')"+x;
      }
      var params = {tableName : deviceId + '_maintenance', orderByClause : 'LOG_DATE', numberOfRowsToBeDelete : json.length};
      _ManageLogSpace(params, function(){
        var sql = "INSERT INTO "+deviceId+"_maintenance(SWITCH_MACHINE_MAINTENANCE_NUM, LOG_DATE, INSPECTION_START_TIME, INSPECTION_END_TIME, INSPECTION_DURATION, INSPECTION_DESCRIPTION, INSPECTION_PERSONNEL, TIME_SINCE_LAST_INSPECTION) " +
        "VALUES " +values;
        connectMaint.query(sql, function (err, result) {
          if (err);
          if(is_initial_poll.charAt(1) == "1" && json.length == 50000){
            insertMaintenanceLog(deviceId, url, json[json.length -1].SWITCH_MACHINE_MAINTENANCE_NUM, is_initial_poll);
          }
        });
      });
    }
  }).catch(function(err) {
  });
}

function saveSwopLogFile(deviceId, url, logFile, is_initial_poll, callback){
  var sql = "select COALESCE(max(id),0) as id from "+deviceId+"_swop";
  connectSwop.query(sql, function (err, rows) {
    if (err);
    else{
      callback(deviceId,url,rows[0].id, is_initial_poll);
    }
  });
}

function insertSwopLog(deviceId, url, result, is_initial_poll){
  if(is_initial_poll.charAt(0) == "1"){
    var numberOfRecords = 50000;
    var query = "update tbl_devices t1 "
    +"inner join tbl_devices t2 "
    +"on t1.id = t2.id "
    +"set t1.initial_poll_processing = CONCAT('1',SUBSTRING(t2.initial_poll_processing,2,4)) "
    +"where t1.id = ?";
    connectSwop.query(query, deviceId, function (err, result) {
      if (err);
    });
  }
  else
  var numberOfRecords = 10000;
  fetch(url + result + "/" + numberOfRecords)
  .catch(function(err) {
    if(is_initial_poll.charAt(0) == "1"){
      query = "update tbl_devices t1 "
      +"inner join tbl_devices t2 "
      +"on t1.id = t2.id "
      +"set t1.initial_poll_processing = CONCAT('0',SUBSTRING(t2.initial_poll_processing,2,4)) "
      +"where t1.id = ?";
      connectSwop.query(query ,deviceId, function (err, result) {
        if (err);
      });
    }
  }).then(function(res) {
    return res.json();
  }).then(function(json) {
    if(json.length >0){
      if(is_initial_poll.charAt(0) == "1" && json.length < 50000){
        query = "update tbl_devices t1 "
        +"inner join tbl_devices t2 "
        +"on t1.id = t2.id "
        +"set t1.initial_poll = CONCAT('0',SUBSTRING(t2.initial_poll,2,4)), "
        +"t1.initial_poll_processing = CONCAT('0',SUBSTRING(t2.initial_poll_processing,2,4)) "
        +"where t1.id = ?";
        connectSwop.query(query ,deviceId, function (err, result) {
          if (err);
        });
      }
      var values='';
      var x='';
      for(var i=0;i<json.length;i++){
        if(i==(json.length-1)){
          x='';
        }else{
          x=',';
        }
        values = values+"("+json[i].id+",'"+json[i].start+"','"+json[i].end+"','"+json[i].opertime+"','"+json[i].mcamps+"','"+json[i].temp+"','"+
        json[i].throwcount+"','"+json[i].time+"')"+x;
      }
      var params = {tableName : deviceId + '_swop', orderByClause : 'time', numberOfRowsToBeDelete : json.length};
      _ManageLogSpace(params, function(){
        var sql = "INSERT INTO "+deviceId+"_swop(id, start, end, opertime, mcamps, temp, throwcount, time) VALUES " +values;
        connectSwop.query(sql, function (err, result) {
          if (err);
          if(is_initial_poll.charAt(0) == "1" && json.length == 50000){
            insertSwopLog(deviceId, url, json[json.length -1].id, is_initial_poll);
          }

        });
      });
    }
  }).catch(function(err) {
  });
}

exports.processEventLog = function(deviceId, pageSize,pageNum, callback){
  var sql = "select id, source, primaryid, secondaryid, level, errtype, time, description from "+deviceId+"_events order by id desc limit "+pageNum*pageSize+","+pageSize;
  connection.query("select count(*) as tot from "+deviceId+"_events",function (err, rows) {
    if (err);
    else{
      var tot = rows[0].tot;
      connection.query(sql, function (err, rows) {
        if (err);
        else{
          callback(rows,tot);
        }
      });
    }
  });
}

exports.processMaintenanceLog = function(deviceId, pageSize,pageNum, callback){
  var sql = "select SWITCH_MACHINE_MAINTENANCE_NUM, LOG_DATE, INSPECTION_START_TIME, INSPECTION_END_TIME, INSPECTION_DURATION, INSPECTION_DESCRIPTION, " +
  "INSPECTION_PERSONNEL, TIME_SINCE_LAST_INSPECTION from "+deviceId+"_maintenance order by SWITCH_MACHINE_MAINTENANCE_NUM desc limit "+pageNum*pageSize+","+pageSize;
  connection.query("select count(*) as tot from "+deviceId+"_maintenance",function (err, rows) {
    if (err);
    else{
      var tot = rows[0].tot;
      connection.query(sql, function (err, rows) {
        if (err);
        else{
          callback(rows,tot);
        }
      });
    }
  });
}

exports.processInstData = function(deviceId, callback){
  var sql =
  "select latch_out_config_setting,position_setting,ipd_mode,part_number,serial_number,system_software_version,comm_processor_version," +
  "vital_fpga_altera_version,vital_fpga_xilinx_version,system_hardware_version,switch_mac_part_num,switch_mac_serial_num," +
  "switch_mac_type,switch_mac_gear_ratio,switch_mac_direction_installation,switch_mac_motor_controller,switch_mac_motor_v,mile_post," +
  "chaining_mode,nominal_position_gps,manufacturing_date,switch_mac_manufacturing_shipping_date,switch_mac_installation_date from " +
  "DEVICE_INST_DATA where DEVICE_ID ="+deviceId;
  connectInst.query(sql, function (err, result) {
    if (err);
    if(result.length>0){
      callback(result);
    }
    else{
      callback([{"latch_out_config_setting":"","position_setting":"","ipd_mode":"","part_number":"","serial_number":"","system_software_version":"",
      "comm_processor_version":"","vital_fpga_altera_version":"","vital_fpga_xilinx_version":"","system_hardware_version":"","switch_mac_part_num":"",
      "switch_mac_serial_num":"","switch_mac_type":"","switch_mac_gear_ratio":"","switch_mac_direction_installation":"","switch_mac_motor_controller":"",
      "switch_mac_motor_v":"","mile_post":"","chaining_mode":"","nominal_position_gps":"","manufacturing_date":"","switch_mac_manufacturing_shipping_date":"",
      "switch_mac_installation_date":""}]);
    }
  });
}

function _ManageLogSpace(params, callback){
  connection.query("select count(id) as totalDevices from tbl_devices;", function(err, rows){
    if(err)
    console.log(err);
    else{
      connection.query("select `VALUE` from config where `KEY` = 'MOTOR_CURR_LOG_MAX_SIZE';", function(error, MaxLogSize){
        if(error)
        console.log(error);
        else{
          var totalDevices = rows[0].totalDevices;
          var maxLogLimit = MaxLogSize[0].VALUE; // In GB
          var maxLimitEachDevice = (maxLogLimit * 1024) / totalDevices; // In MB
          DeleteLogRecursively(maxLimitEachDevice, params.tableName, params.orderByClause, params.numberOfRowsToBeDelete, callback);
        }
      });
    }
  });
}

function DeleteLogRecursively(maxLimitEachDevice, tableName, orderByClause, numberOfRowsToBeDelete, callback){
  var query = "SELECT sum(round(((data_length + index_length) / 1024 / 1024), 2)) `log_size` FROM information_schema.TABLES WHERE table_schema = 'ieccboss' AND table_name = '" + tableName + "';";
  connection.query(query, function(errorInLogSize, ActualLogSize){
    if(errorInLogSize)
    console.log(errorInLogSize);
    else{
      var currentLogSize = ActualLogSize[0].log_size; // In MB
      if(currentLogSize > maxLimitEachDevice){
        connection.query('DELETE FROM ' + tableName + ' order by ' + orderByClause + ' asc LIMIT ' + numberOfRowsToBeDelete + '',function(errorInDelete){
          if(errorInDelete)
          console.log(errorInDelete);
          else{
            DeleteLogRecursively(maxLimitEachDevice, tableName, orderByClause, numberOfRowsToBeDelete, callback);
          }
        })
      }
      else{
        callback();
      }
    }
  });
}

function maintainenceCronIO(deviceId,url,result){
  var socket = require('socket.io-client')(url);


  socket.on('connect', function(socket){
  });
  socket.on('message', function(message) {
  });
  socket.emit('getInstData','172.18.42.1');
  socket.on('getInstData',function(message){
    socket.emit('getMaintData','172.18.42.1',result);
    socket.on('sendMaintData',function(json){
      for(var i=0;i<json.length;i++){
        var sql = "INSERT INTO "+deviceId+"_maintenance(SWITCH_MACHINE_MAINTENANCE_NUM, LOG_DATE, INSPECTION_START_TIME, INSPECTION_END_TIME, INSPECTION_DURATION, INSPECTION_DESCRIPTION, INSPECTION_PERSONNEL, TIME_SINCE_LAST_INSPECTION) " +
        "VALUES ("+json[i].SWITCH_MACHINE_MAINTENANCE_NUM+","+json[i].LOG_DATE+","+json[i].INSPECTION_START_TIME+","+json[i].INSPECTION_END_TIME+","
        +json[i].INSPECTION_DURATION+","+json[i].INSPECTION_DESCRIPTION+",'"+
        json[i].INSPECTION_PERSONNEL+"','"+json[i].TIME_SINCE_LAST_INSPECTION+"')";
        connection.query(sql, function (err, result) {
          if (err);
        });
      }
    });
  });
}

function eventsCronIO(deviceId,url,result){
  var socket = require('socket.io-client')(url);
  socket.on('connect', function(socket){
  });
  socket.on('message', function(message) {
  });
  socket.emit('getInstData','172.18.42.1');
  socket.on('getInstData',function(message){
    socket.emit('getMaintData','172.18.42.1',result);
    socket.on('sendMaintData',function(json){
      for(var i=0;i<json.length;i++){
        var sql = "INSERT INTO "+deviceId+"_maintenance(SWITCH_MACHINE_MAINTENANCE_NUM, LOG_DATE, INSPECTION_START_TIME, INSPECTION_END_TIME, INSPECTION_DURATION, INSPECTION_DESCRIPTION, INSPECTION_PERSONNEL, TIME_SINCE_LAST_INSPECTION) " +
        "VALUES ("+json[i].SWITCH_MACHINE_MAINTENANCE_NUM+","+json[i].LOG_DATE+","+json[i].INSPECTION_START_TIME+","+json[i].INSPECTION_END_TIME+","
        +json[i].INSPECTION_DURATION+","+json[i].INSPECTION_DESCRIPTION+",'"+
        json[i].INSPECTION_PERSONNEL+"','"+json[i].TIME_SINCE_LAST_INSPECTION+"')";
        connection.query(sql, function (err, result) {
          if (err);
        });
      }
    });
  });
}

function swopCronIO(deviceId,url,result){
  var socket = require('socket.io-client')(url);
  socket.on('connect', function(socket){
  });
  socket.on('message', function(message) {
  });
  socket.emit('getInstData','172.18.42.1');
  socket.on('getInstData',function(message){
    socket.emit('getMaintData','172.18.42.1',result);
    socket.on('sendMaintData',function(json){
      for(var i=0;i<json.length;i++){
        var sql = "INSERT INTO "+deviceId+"_maintenance(SWITCH_MACHINE_MAINTENANCE_NUM, LOG_DATE, INSPECTION_START_TIME, INSPECTION_END_TIME, INSPECTION_DURATION, INSPECTION_DESCRIPTION, INSPECTION_PERSONNEL, TIME_SINCE_LAST_INSPECTION) " +
        "VALUES ("+json[i].SWITCH_MACHINE_MAINTENANCE_NUM+","+json[i].LOG_DATE+","+json[i].INSPECTION_START_TIME+","+json[i].INSPECTION_END_TIME+","
        +json[i].INSPECTION_DURATION+","+json[i].INSPECTION_DESCRIPTION+",'"+
        json[i].INSPECTION_PERSONNEL+"','"+json[i].TIME_SINCE_LAST_INSPECTION+"')";
        connection.query(sql, function (err, result) {
          if (err);
        });
      }
    });
  });
}
