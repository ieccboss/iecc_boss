var config_connection_obj = require('../db_connection.json');
var mysql = require('mysql');
var connection = mysql.createConnection(config_connection_obj);
connection.connect();

exports.authenticate = function(req, res){
    var db = req.app.get('db');
    var responseJSON = {};
    var uname = req.body.username;
    var password = req.body.password;
    
    function getResFromDb(uname, callback) {
    	connection.query("SELECT password FROM USER_ACCOUNT where username='"+uname+"'", function(err, result) {
    	if(err)
    	  callback(err, null);
    	else if( !result )
    	  callback('fail','no record');
    	 else if( result != null && result[0] != null ){
    	  callback('pass', result[0].password);
    	}
    	else
    	  callback('fail','no record');
    	}.bind(this));
    	}
    
    if(typeof req.session.user == 'undefined' || req.session.user == null){
    	
    	getResFromDb(uname, processResult);
    	
    	
    } else{
            responseJSON.status='208';
   }
function processResult(err, result) {
    if(err=='pass'){
        if(password==result){
            req.session.user = uname;
          //  console.log('user auth session created req.session.user = '+req.session.user);
            responseJSON.status='200';
        }else{
            responseJSON.status='404';
        }
    } else if(err=='fail'){
        responseJSON.status='404';
    } else {
        responseJSON.status='500';
    }
    res.send(responseJSON);
 }
//getResFromDb(uname, processResult);
};

exports.checkSession = function(req, res){
    var responseJSON = {};
    if(typeof req.session == 'undefined' || req.session == null || typeof req.session.user == 'undefined' || req.session.user == null){
        responseJSON.status='404';
        res.send(responseJSON);
    }else{
        responseJSON.status='208';
        res.send(responseJSON);
    }
};

exports.renewSession = function(req, res){	
    var responseJSON = {};
   // console.log('renewSession');
    if(typeof req.session == 'undefined' || req.session == null || typeof req.session.user == 'undefined' || req.session.user == null){
        responseJSON.status='404';
    //    console.log('renewSession'+responseJSON.status);
        res.send(responseJSON);
    }else{
    	req.session.touch();
        responseJSON.status='200';
     //   console.log('renewSession'+responseJSON.status);
        res.send(responseJSON);
    }
};

exports.endSession = function(req, res){
    var responseJSON = {};
    if(typeof req.session.user == 'undefined' || req.session.user == null){
        responseJSON.status='404';
        req.session.user=null;
        req.session.destroy();
        req.watcher = null;
        res.send(responseJSON);
    }else{
    	
    	req.session.destroy(function (err) {
    	    if (err) {
    	    }
    	  });
    	  
        responseJSON.status='200';
     //   req.session.user=null;
    //    req.session.destroy();
        req.watcher = null;       
        res.send(responseJSON);
        
        
    }
};

exports.authDeviceConfig = function(req, res){
    var responseJSON = {};
    if(typeof req.session.user == 'undefined' || req.session.user == null){
         res.render('index.ejs',{watcher: req.session.user});
    }else{
        res.render('deviceConfiguration.ejs',{watcher: req.session.user});
    }
};