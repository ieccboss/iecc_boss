//$.noConflict();
jQuery( document ).ready(function( $ ) {
	var selectedDevice;
	var categoryToDelete;
	var deviceToDelete;
	var expandedCategories = [];
	var socket = io();
	var deviceColorPrority = {
		"green" : 1,
		"yellow" : 2,
		"red" : 3,
		"gray" : 4
	}

	socket.emit('get-tree-data');
	socket.on('tree_data', function(response) {
		CreateTree(response.data);
	});

	// function to create caterory tree
	function CreateTree(data){
		$("#tree").empty();
		var tree = '<ul id="tree">';
		var objCategoryList = JSON.parse(data);
		for (var category in objCategoryList) {
			if (objCategoryList.hasOwnProperty(category)) {
				tree += '<li dropable="true" class="droptarget" style="font-weight:bold;" id="C'+ objCategoryList[category].cat_id +'" name="'+ objCategoryList[category].cat_id +'">' + objCategoryList[category].cat_name;
				tree +='<input type="hidden" id="hidden'+ objCategoryList[category].cat_id +'" value="'+objCategoryList[category].cat_name+'"></input>'
				var objNode = objCategoryList[category].node;
				if(objNode.length > 0){
					tree += '<ul>';
				}
				for (var device in objNode) {
					if (objNode.hasOwnProperty(device)) {
						tree += '<li id="D'+ objNode[device].dev_id +'" name="'+ objNode[device].dev_id +'" class="draggable" draggable="true" style="cursor:pointer !important"><div class="draggableSymbol"><p></p></div><a id="L'+ objNode[device].dev_id +'" value="'+ objNode[device].dev_name +'" class="iecc-device">'+ objNode[device].dev_name +'</a></li>'
					}
				}
				if(objNode.length > 0){
					tree += '</ul>';
				}
				+'</li>';
			}
		}
		tree += '</ul> <p id="demo"></p>';
		$("#treeCcontainer").prepend(tree);

		$('.droptarget').contextmenu({
			target: "#context-menu",
			menuIdentifier : "droptarget",
			before : function(event, context){
				console.log(context[0].id);
				categoryToDelete = context[0].id;
				return true;
			}
		});

		$('.iecc-device').contextmenu({
			target: "#context-menu-device",
			menuIdentifier : "draggable",
			before : function(event, context){
				console.log(context[0].id);
				deviceToDelete = context[0].id;
				return true;
			}
		});

		$("#btn-detete-category").click(function(e) {
			if(categoryToDelete){
				if(document.getElementById('hidden' + categoryToDelete.replace('C','')).value.toLowerCase() == 'unassigned'){
					ShowAlert('<span style="font-weight:bold">UNASSIGNED</span> category can not be deleted.');
				}
				else{
					
					ShowConfirm('Are you sure you want to delete category : <span style="font-weight:bold">' + document.getElementById('hidden' + categoryToDelete.replace('C','')).value.toUpperCase() +'</span> ?', function(){
						socket.emit("delete-category", categoryToDelete.replace('C',''), function(response){
							ShowAlert(response.message);
							debugger;
							if(response.status == "ok"){
								socket.emit('get-tree-data');
							}
						})
					});

				}
			}
		});

		$("#btn-detete-device").click(function(e) {
			if(deviceToDelete){
				ShowConfirm('Are you sure you want to delete device : <span style="font-weight:bold">' + document.getElementById(deviceToDelete).innerHTML.toUpperCase() +'</span> ? All data for this device will be delete and can not be recovered.', function(){
					socket.emit("delete-device", deviceToDelete.replace('L',''), function(response){
						ShowAlert(response.message);
						debugger;
						if(response.status == "ok"){
							socket.emit('get-tree-data');
						}
					})
				});

			}
		});


		$(document).ready(function() {
			//var tree = $('#tree').goodtree({'setFocus': $('.focus')});
			var tree = $('#tree').goodtree({'reveal': $('.focus')}, MaintainState);
		});

		for (var category in objCategoryList) {
			if (objCategoryList.hasOwnProperty(category)) {
				var categoryColor = "";
				var objNode = objCategoryList[category].node;
				if(objNode.length <= 0){
					document.getElementById('C'+ objCategoryList[category].cat_id).style.marginLeft = "10px";
				}
				for (var device in objNode) {
					if (objNode.hasOwnProperty(device)) {
						$('#D'+objNode[device].dev_id).on('click', function (){
							GetdeviceData(this.id.replace('D',''));
						});
						var colorClass = getDeviceColor(objNode[device]);
						var categoryColor = categoryColor == "" ? colorClass : (deviceColorPrority[categoryColor] > deviceColorPrority[colorClass] ? categoryColor : colorClass);
						$('#D'+objNode[device].dev_id).removeClass().addClass('draggable');
						$('#D'+objNode[device].dev_id).addClass(colorClass);
						$('#D'+objNode[device].dev_id).on("dragstart", Dragstart);
						$('#D'+objNode[device].dev_id).on("dragend", Dragend);
					}
				}
				$('#C'+objCategoryList[category].cat_id).removeClass().addClass('droptarget');
				$('#C'+objCategoryList[category].cat_id).addClass('category-' + categoryColor);
				$('#C'+objCategoryList[category].cat_id).on("drop", Drop);
				$('#C'+objCategoryList[category].cat_id).on("dragover", Dragover);
				$('#C'+objCategoryList[category].cat_id).on("dragleave", Dragleave);
				$('#C'+objCategoryList[category].cat_id).on("dragenter", Dragenter);
			}
		}
	}

	socket.on('refresh-colors', function(response) {
		console.log('refresh color called');
		BindDeviceAndCategoryColors(response.data);
	});

	function BindDeviceAndCategoryColors(data){
		var objCategoryList = JSON.parse(data);
		for (var category in objCategoryList) {
			if (objCategoryList.hasOwnProperty(category)) {
				var categoryColor = "";
				var objNode = objCategoryList[category].node;
				for (var device in objNode) {
					if (objNode.hasOwnProperty(device)) {
						var colorClass = getDeviceColor(objNode[device]);
						var categoryColor = categoryColor == "" ? colorClass : (deviceColorPrority[categoryColor] > deviceColorPrority[colorClass] ? categoryColor : colorClass);
						$('#D'+objNode[device].dev_id).removeClass().addClass('draggable');
						$('#D'+objNode[device].dev_id).addClass(colorClass);
					}
				}
				$('#C'+objCategoryList[category].cat_id).removeClass().addClass('droptarget');
				$('#C'+objCategoryList[category].cat_id).addClass('category-' + categoryColor);
			}
		}

	}
	// function maintain state of categories.
	function MaintainState(expandedNodeList){
		for (var category in expandedNodeList){
			$('#'+ expandedNodeList[category] +' .toggle').removeClass('closed').addClass('open');
			$('#'+ expandedNodeList[category] +' ul').css("display", "");
		}
	}
	// function to get device information on click
	function GetdeviceData(deviceID){
		selectedDevice = deviceID;
		document.getElementById("hidden_selected_node").value = deviceID;
		socket.emit('get-device-info', deviceID, function(response){
			var data = response.data;
			document.getElementById("lbl-device-name").textContent = data.name;
		//	document.getElementById("lbl-device-location").textContent = data.device_location;
		//	document.getElementById("lbl-software-version").textContent = data.software_version;
		//	document.getElementById("lbl-hardware-version").textContent = data.hardware_version;
		document.getElementById("lbl-device-ip").textContent = data.ip;
		document.getElementById("lnk_ip").href = data.ip;
		document.getElementById("lbl-switch-direction").textContent = data.switch_direction;
		document.getElementById("lbl-health-status").textContent = data.health_status;
		document.getElementById("lbl-boss-entry-date").textContent = data.created_on;
		$("#longTermLogs").showInstData({param1: deviceID, param2: data.ip, param3: data.latch_out_status});
	});
	}

	// function to get device final color based on color priorities.
	function getDeviceColor(deviceData){
		var deviceColor ="";
		for (var i = 3; i < Object.keys(deviceData).length - 1 ; i++){
			var highPriorityColor = (deviceColorPrority.hasOwnProperty(deviceData[Object.keys(deviceData)[i]]) ? deviceColorPrority[deviceData[Object.keys(deviceData)[i]]] : 0) > (deviceColorPrority.hasOwnProperty(deviceData[Object.keys(deviceData)[i+1]]) ? deviceColorPrority[deviceData[Object.keys(deviceData)[i+1]]] : 0) ? deviceColorPrority[deviceData[Object.keys(deviceData)[i]]] :  deviceColorPrority[deviceData[Object.keys(deviceData)[i+1]]];
			deviceColor = deviceColor == "" ? Object.keys(deviceColorPrority).find(function(key){return deviceColorPrority[key] === highPriorityColor}) : (deviceColorPrority[deviceColor] > highPriorityColor ? deviceColor : Object.keys(deviceColorPrority).find(function(key){return deviceColorPrority[key] === highPriorityColor}));
		}
		return deviceColor;
	}

	// function to apply drag and drop on tree nodes.
	/* Events fired on the drag target */
	function Dragstart(event) {
		var crt = event.target.cloneNode(true);
		//crt.style.backgroundColor = "red";
		// The dataTransfer.setData() method sets the data type and the value of the dragged data
		event.originalEvent.dataTransfer.setData("Text", event.target.id);
		// Output some text when starting to drag the p element
		// document.getElementById("demo").innerHTML = "Started to drag the p element.";
		// Change the opacity of the draggable element
		//event.target.style.opacity = "0.4";
	}
	// Output some text when finished dragging the p element and reset the opacity
	function Dragend(event) {
		//document.getElementById("demo").innerHTML = "Finished dragging the p element.";
		//event.target.style.opacity = "1";
	}

	// When the draggable p element enters the droptarget, change the DIVS's border style
	function Dragenter(event) {
		event.stopPropagation(); // might not be necessary
		event.preventDefault();
		if ( event.target.classList.contains("droptarget")) {
			//event.target.style.border = "3px dotted red";
			if(!event.target.classList.contains('allowDropBackground'))
				event.target.classList.add("allowDropBackground");
		}
		return false;
	}
	// When the draggable p element leaves the droptarget, reset the DIVS's border style
	function Dragleave(event) {
		if ( event.target.classList.contains("droptarget") ) {
			//event.target.style.border = "";
			if(event.target.classList.contains('allowDropBackground'))
				event.target.classList.remove("allowDropBackground");
		}
	}
	// By default, data/elements cannot be dropped in other elements. To allow a drop, we must prevent the default handling of the element
	function Dragover(event) {
		event.stopPropagation(); // might not be necessary
		event.preventDefault();
		return false;
	}
	/* On drop - Prevent the browser default handling of the data (default is open as link on drop)
	Reset the color of the output text and DIV's border color
	Get the dragged data with the dataTransfer.getData() method
	The dragged data is the id of the dragged element ("drag1")
	Append the dragged element into the drop element
	*/
	function Drop(event) {
		event.preventDefault();
		if ( event.target.classList.contains("droptarget") ) {
			document.getElementById("demo").style.color = "";
			event.target.style.border = "";
			var categoryID = event.target.id.replace('C','');
			var deviceID = event.originalEvent.dataTransfer.getData("Text").replace('D','');
			UpdateCategoriesList(categoryID, deviceID);
			//var data = event.dataTransfer.getData("Text");
			//event.target.appendChild(document.getElementById(data));
		}
	}

	// function to update caterories in DB.
	function UpdateCategoriesList(categoryID, deviceID){
		var updateParems = {categoryID : categoryID, deviceID: deviceID};
		socket.emit('update-categories', updateParems);
	}

	// function to open and close category input form.
	function ToggleCategoryForm(){
		if(document.getElementById("btn_show_category_form").style.display == "block"){
			document.getElementById("btn_show_category_form").style.display = "none";
			document.getElementById("btn_hide_category_form").style.display = "block";
			document.getElementById("category_from").style.display = "block";
		}
		else{
			document.getElementById("btn_show_category_form").style.display = "block";
			document.getElementById("btn_hide_category_form").style.display = "none";
			document.getElementById("category_from").style.display = "none";
		}
	}

	// function to add new category.
	function SaveCategory(){
		if(document.getElementById('txtCategoryName').value.trim() != ""){
			var categoryNmae = document.getElementById('txtCategoryName').value;
			socket.emit('save-category', categoryNmae, function(response){
				if(response.status == 'ok'){
					ShowAlert('Category successfully saved.')
					document.getElementById('txtCategoryName').value ="";
					socket.emit('get-tree-data');
					ToggleCategoryForm();
				}
				else {
					ShowAlert(response.message);
				}
			});
		}
		else {
			ShowAlert("Category name can not be blank.");
		}
	}

	// bind functions with their corresponding events.
	document.getElementById("btn_show_category_form").onclick = ToggleCategoryForm;
	document.getElementById("btn_hide_category_form").onclick = ToggleCategoryForm;
	document.getElementById("btn_save_category").onclick = SaveCategory;

	// handle resizable panels.
	$(".panel-left").resizable({
		handleSelector: ".splitter",
		resizeHeight: false
	});
	$(".panel-top").resizable({
		handleSelector: ".splitter-horizontal",
		resizeWidth: false
	});
	
	$("#editDevInfo").click(function(e) {
		if (selectedDevice) {
			$("#editDevInfo").css("display", "none");
			$("#lnk_ip").css("display", "none");
			$("#lbl-device-name").css("display", "none");
			$("#cancelEditDevInfo").css("display", "block");
			$("#saveDevInfo").css("display", "block");
			$("#device_name").css("display", "block");
			$("#ipContainer").css("display", "block");
			$("#deviceCheckContainer").css("display", "block");
			$("#device_name").val($("#lbl-device-name").text());
			var link = $("#lnk_ip").text().substring($("#lnk_ip").text().indexOf('//')+2);
			$("#deviceIP1").val(link.substring(0,link.indexOf('.')));
			link = link.substring(link.indexOf('.')+1);
			$("#deviceIP2").val(link.substring(0,link.indexOf('.')));
			$("#deviceIP3").val(link.substring(link.indexOf('.')+1,link.lastIndexOf('.')));
			$("#deviceIP4").val(link.substring(link.lastIndexOf('.')+1,link.lastIndexOf(':')));
			$("#device_port").val(link.substring(link.indexOf(':')+1));
		} else {
			ShowAlert('Please Select a Device First');
		}
	});
	$("#cancelEditDevInfo").click(function(e) {
		$("#cancelEditDevInfo").css("display", "none");
		$("#saveDevInfo").css("display", "none");
		$("#ipContainer").css("display", "none");
		$("#device_name").css("display", "none");
		$("#deviceCheckContainer").css("display", "none");
		$("#editDevInfo").css("display", "block");
		$("#lnk_ip").css("display", "block");
		$("#lbl-device-name").css("display", "block");

	});
	$("#saveDevInfo").click(
		function() {
			var devName = $("#device_name").val();
			if ($("#deviceIP1").val() && $("#deviceIP2").val() && $("#deviceIP3").val() && $("#deviceIP4").val()) {
				var ip = $("#deviceIP1").val() + '.'
				+ $("#deviceIP2").val() + '.'
				+ $("#deviceIP3").val() + '.'
				+ $("#deviceIP4").val();
				var port = $("#device_port").val();
				var NameValidation = document.getElementById("lbl-device-name").textContent.toLowerCase().trim() == devName.toLowerCase().trim() ? false : true;
				var IpValidation = $("#lnk_ip").text().split('//')[1].split(':')[0].trim() == ip ? false : true;

				if (devName && port && ip) {
					var deviceData = {
						devId : selectedDevice,
						ip : ip,
						port : port,
						devName : devName,
						isValidateName : NameValidation,
						isValidateIp : IpValidation
					};
					alreadyFetched = {};
					socket.emit('update-device-info', deviceData, function(data){
						if(data.status == 'ok'){
							showDevInfoAfterUpdate();
						}
						else{
							ShowAlert(data.message);
						}
					});
				} else {
					if (devName)
						ShowAlert('Please enter Port Number');
					else
						ShowAlert('Please enter Device Name');
				}
			} else {
				ShowAlert('Please check IP and then try again');
			}
		});
	function showDevInfoAfterUpdate(){
		ShowAlert('Device information updated successfully');
		GetdeviceData(selectedDevice);
	}
});
