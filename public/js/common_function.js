  // Instance variables.
  var messagePopup = new Popup('message');
  messagePopup.hide();

  // 1. Function to detect Microsoft family browsers.
  function IsIEorEDGE(){
    if (/MSIE 10/i.test(navigator.userAgent)) {
      // This is internet explorer 10
      return true;
    }

    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
      // This is internet explorer 9 or 11
      return true;
    }

    if (/Edge\/\d./i.test(navigator.userAgent)){
      // This is Microsoft Edge
      return true;
    }
    return false;
  }

  // 2. Function to show alert message.
  function ShowAlert(alertMessage, callback){
    var popupParems;
    if(callback){
      popupParems = {
        title : 'Message',
        message : ('<p style="text-align:center">' + alertMessage + '</p>'),
        width : '370px',
        buttons : [{
          title : 'OK',
          action : callback
        }]
      }
    }
    else{
      popupParems = {
        title : 'Message',
        message : ('<p style="text-align:center">' + alertMessage + '</p>'),
        width : '370px'
      }
    }
    messagePopup.show(popupParems);
  }

  // 3. Function to save json object in local storage.
  function SaveToLocalStorage(key, obj){
    window.localStorage.setItem(key, JSON.stringify(obj));
  }

  // 4. Function to get object from local storage.
  function GetFromLocalStorage(key){
    if(window.localStorage[key])
    return JSON.parse(window.localStorage[key]);
    else return false;
  }

  // 5. Function to remove object from local storage.
  function RemoveFromLocalStorage(obj){
    window.localStorage.removeItem(obj);
  }

  // 6. Function to clear local storage
  function ClearLocalStorage(){
    window.localStorage.clear();
  }

  // 7. Function to handel tab selection

 function ManageTabSelection(self){
   jQuery('#motor_current').removeClass('selected-tab');
   jQuery('#device_data').removeClass('selected-tab');
   jQuery('#swop_log').removeClass('selected-tab');
   jQuery('#event_log').removeClass('selected-tab');
   jQuery('#maintenance_log').removeClass('selected-tab');
   jQuery('#snmp_log').removeClass('selected-tab');
   jQuery('#specific_swop_log').css("display","none");
   jQuery('#specific_event_log').css("display","none");
   jQuery('#specific_graph').css("display","none");
   jQuery('#specific_maintenance_log').css("display","none");
   jQuery('#specific_snmp_log').css("display","none");
   jQuery(self).addClass('selected-tab');
   if(jQuery(self)[0].id == "device_data"){
	   jQuery("#cancelEditDevInfo").css("display", "none");
	   jQuery("#saveDevInfo").css("display", "none");
	   jQuery("#ipContainer").css("display", "none");
	   jQuery("#device_name").css("display", "none");
	   jQuery("#deviceCheckContainer").css("display", "none");
	   jQuery("#editDevInfo").css("display", "block");
	   jQuery("#lnk_ip").css("display", "block");
	   jQuery("#lbl-device-name").css("display", "block");
	     jQuery('#instData').css("display","block");
	   }
   else if(jQuery(self)[0].id == "swop_log"){
     jQuery('#specific_swop_log').css("display","block");
   }
   else if(jQuery(self)[0].id == "motor_current"){
     jQuery('#motor_current').css("display","block");
   }
   else if(jQuery(self)[0].id == "event_log"){
	   //  jQuery('#event_log').css("display","block");
	     jQuery('#specific_event_log').css("display","block");
	   }
   else if(jQuery(self)[0].id == "maintenance_log"){
	     jQuery('#specific_maintenance_log').css("display","block");
	   }
   else if(jQuery(self)[0].id == "snmp_log"){
	     jQuery('#specific_snmp_log').css("display","block");
	   }
   
 }

 // 8. Function to show alert message.
  function ShowConfirm(message, actionOnYes){
    var popupParems;
      popupParems = {
        title : 'Message',
        message : ('<p style="text-align:center">' + message + '</p>'),
        width : '370px',
        buttons : [{
          title : 'Yes',
          action : actionOnYes
        },
        {
          title : 'No',
          action : messagePopup.hide
        }]
      }
    messagePopup.show(popupParems);
  }
  
  
  
  function ShowSessionConfirm(message, actionOnYes, actionOnNo){
	    var popupParems;
	      popupParems = {
	        title : 'Message',
	        message : ('<p style="text-align:center">' + message + '</p>'),
	        width : '370px',
	        buttons : [{
	          title : 'Yes',
	          action : actionOnYes
	        },
	        {
	          title : 'No',
	          action : actionOnNo
	        }]
	      }
	    messagePopup.show(popupParems);
	  }
  
  
  
