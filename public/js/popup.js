var Popup = function(id) {
	var _self = this;

	if (!id) {
		return null;
	}

	var html = '';
	html += '<div class="section">';
	html += '	<h1 class="popup-title"></h1>';
	html += '	<div class="section-content clearfix popup-content">';
	html += '	</div>';
	html += '	<div class="section-toolbar clearfix">';
	html += '		<div class="popup-buttons" style="float: right">';
	html += '		</div>';
	html += '	</div>';
	html += '</div>';

	this.overlay = $('overlay');
	if (!this.overlay) {
		this.overlay = new Element('div', {
			id : 'overlay'
		});
		$(document.body).insert(this.overlay);
		this.overlay.setOpacity(0.7);
		this.overlay.hide();
	}

	this.el = new Element('div', {
		id : id,
		className : 'popup'
	});
	$(document.body).insert(this.el);
	this.el.update(html);
	this.el.hide();

	this.titleEl = this.el.down('.popup-title');
	this.contentEl = this.el.down('.popup-content');
	this.buttonsEl = this.el.down('.popup-buttons');

	this.show = this.open = function(props) {
		props.width = props.width || 350;
		props.title = props.title || 'Popup';
		props.message = props.message || 'Message';
		props.buttons = props.buttons || [ {
			title : 'OK',
			action : this.close
		} ];

		this.titleEl.update(props.title);
		this.contentEl.update(props.message);
		this.buttonsEl.update();

		props.buttons.each(function(b) {
			b.action = b.action || _self.close;

			var actionLink = '#popup-action';
			var dynamicAction = true;

			if (typeof b.action == 'string') {
				actionLink = b.action;
				dynamicAction = false;
			}

			var buttonEl = new Element('a', {
				href : actionLink
			}).addClassName('button').update(b.title);

			if (dynamicAction) {
				buttonEl.on('click', function(evt) {
					evt.stop();
					b.action.bind(_self)();
				});
			}

			_self.buttonsEl.insert(buttonEl);
		});

		var isStrWidth = typeof (props.width) === 'string';
		var numWidth, units;
		if (isStrWidth) {
			var parts = /^([0-9]+)(.*)$/.exec(props.width);
			numWidth = parts[1], units = parts[2];
		} else {
			numWidth = props.width, units = 'px';
		}

		this.el.setStyle({
			width : isStrWidth ? props.width : numWidth + units,
			marginLeft : -(numWidth / 2) + units
		});

		if (props.top) {
			this.el.setStyle({top: props.top});
		}

		this.overlay.show();
		this.el.show();

		if (!(props || props.flexHeight)) {
			this.contentEl[this.el.getHeight() > 300 ? 'addClassName' : 'removeClassName']('popup-height-adjust');
		}

		/* ಠ_ಠ */
		$$('.ie6 select').invoke('setStyle', {
			visibility : 'hidden'
		});
	};

	this.hide = this.close = function() {
		this.overlay.hide();
		this.el.hide();

		/* ಠ_ಠ */
		$$('.ie6 select').invoke('setStyle', {
			visibility : 'visible'
		});
	};
};

Popup.frobIEElements = function() {
	if (Prototype.Browser.IE) {
		$$('.ie6 .section-toolbar', '.ie7 .section-toolbar').invoke('hide').invoke('show');
	}
};
