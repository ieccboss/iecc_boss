jQuery( document ).ready(function( $ ) {
  var socket = io();
  var perpage = 25;
  var page = 1;
  var maxpage = 1;
  var totalcount = 0;

  document.getElementById('snmpPageSize').value = 25;
  document.getElementById('snmpPageNum').value = 1;

  //listen to socket events.
  socket.on('snmp_log', function(data) {
      /*event_logs_data.eventLogs = JSON.parse(event_logs_data.eventLogs);
      for(var i = 0; i < event_logs_data.eventLogs.length; i++){
        event_logs_data.eventLogs[i].Index = perpage * (page - 1) + (i + 1);
      }
      last_transport = event_logs_data;*/
      totalcount = data.total;
      if(totalcount > 0){
        maxpage = Math.ceil(totalcount/perpage);
        document.getElementById('snmpPageNum').setAttribute("max",maxpage);
        document.getElementById('snmp-total-page-num').innerHTML = maxpage;
        if( document.getElementById('snmpPageNum').value > maxpage ){
          document.getElementById('snmpPageNum').value = maxpage;
          page = maxpage;
          refreshData();
        }
        bindData(data.snmp_logs);
      }
      else{
        maxpage = 1;
        page =1;
        document.getElementById('snmpPageNum').setAttribute("max",1);
        document.getElementById('snmp-total-page-num').innerHTML = 1;
        document.getElementById('snmpPageNum').value = 1;
        bindNoData();
      }
  });


  function refreshData(){
    var _recordsPerPage = document.getElementById('snmpPageSize').value;
    var _pageNumber = document.getElementById('snmpPageNum').value;
    debugger;
    if(document.getElementById("hidden_selected_node").value == ""){
      ShowAlert('Please select a device first.');
    }
    else{
      socket.emit('get_snmp_logs', { recordsPerPage: _recordsPerPage, pageNumber : _pageNumber, tableName : document.getElementById("hidden_selected_node").value+'_snmp' });
    }
  }

  function bindData(snmp_logs) {
    drawTable(snmp_logs);
  }

  function bindNoData(){
    var table = document.getElementById("snmpDataTbl");
    while(table.rows.length > 0) {
      table.deleteRow(0);
    }

    var row = table.insertRow();
    var cell;

    cell = row.insertCell(0);
    cell.innerHTML= "No Records.";
    cell.colspan = "8";
  }

  function drawTable(data) {
    var table = document.getElementById("snmpDataTbl");
    while(table.rows.length > 0) {
      table.deleteRow(0);
    }
    /* add sorting by sortcolumn and sortdirect */
    /*data.sort(function(a,b) {
      var item1 = a[Object.keys(a)[sortcolumn]];
      var item2 = b[Object.keys(b)[sortcolumn]];
      var num1 = parseInt(a[Object.keys(a)[6]]);
      var num2 = parseInt(b[Object.keys(b)[6]]);
      if (sortcolumn == 5) {
        item1 = num1;
        item2 = num2;
      }
      if (item1 > item2) return sortdirect;
      if (item1 < item2) return -1*sortdirect;
      return 0;
    });
    pageDataToSave = (JSON.parse(JSON.stringify(data))); // to clone the object.*/
    for (var i = 0; i < data.length; i++) {
      drawRow(data[i]);
    }
  }

  function drawRow(rowData) {

    var table = document.getElementById("snmpDataTbl");
    var row = table.insertRow();
    var cell;

    cell = row.insertCell(0);
    cell.innerHTML= rowData.rowid;
    cell.style.width = '9%';

    cell = row.insertCell(1);
    cell.innerHTML= rowData.text_value;
    cell.style.width = '41%';
    //cell.className = 'logpage-img';

    cell = row.insertCell(2);
    cell.innerHTML= rowData.numeric_id;
    cell.style.width = '20%';
    //cell.className = 'logpage-num';

    cell = row.insertCell(3);
    cell.innerHTML= rowData.time;
    cell.style.width = '15%';
    //cell.className = 'logpage-time';

    cell = row.insertCell(4);
    cell.innerHTML= rowData.criticality;
    cell.style.width = '14%';
    //cell.className = 'logpage-event';
  }

  function checkPerPage() {
    var new_perpage = document.getElementById('snmpPageSize').value;
    var min_val = document.getElementById('snmpPageSize').readAttribute("min");
    //var max_val = $('snmpPageSize').readAttribute("max");
    var max_val = 100;
    var intperPage = parseInt(new_perpage);
    if ( (intperPage.toString() != new_perpage) || intperPage < min_val || intperPage > max_val) {
      ShowAlert ("Items per page should be integer value in range " + min_val + " to " + max_val);
      document.getElementById('snmpPageSize').value = perpage;
      return false;
    }
    perpage = intperPage;
    if ((page-1)*perpage >= totalcount) {
      page = Math.ceil(totalcount/perpage);
      document.getElementById('snmpPageNum').value = page;
    }
    return true;
  }

  function checkPage() {
    var new_page = document.getElementById('snmpPageNum').value;
    var intPage = parseInt(new_page);
    if ( (intPage.toString() != new_page) || intPage < 1 || intPage > maxpage) {
      ShowAlert ("Page should be integer value in range 1 to " + maxpage);
      document.getElementById('snmpPageNum').value = page;
      return false;
    }
    page = intPage;
    return true;
  }

  $('#snmpPageSize').change('change',function() {
    if (checkPerPage()) {
      refreshData();
    }
  });

  $('info_refresh_btn').click(function() {
    refreshData();
  });

  $('#snmpPageNum').change(function() {
    if (checkPage()) {
      refreshData();
    }
  });
  $('#snmpLogFirst').click( function() {
    if (page > 1) {
      page = 1;
      document.getElementById('snmpPageNum').value = page;
      refreshData();
    }
  });
  $('#snmpLogPrev').click(function() {
    if (page > 1) {
      page = page - 1;
      document.getElementById('snmpPageNum').value = page;
      refreshData();
    }
  });
  $('#snmpLogNext').click(function() {
    debugger;
    if (page < (Math.ceil(totalcount/perpage))) {
      page = page + 1;
      document.getElementById('snmpPageNum').value = page;
      refreshData();
    }
  });
  $('#snmpLogLast').click(function() {
    if (page < (Math.ceil(totalcount/perpage))) {
      page = Math.ceil(totalcount/perpage);
      document.getElementById('snmpPageNum').value = page;
      refreshData();
    }
  });

  $("#snmp_log").click(function() {
    if(!this.classList.contains("selected-tab")){
      document.getElementById('instData').style.display = 'none';
      document.getElementById('eventLog').style.display = 'none';
      document.getElementById('swopLog').style.display = 'none';
      document.getElementById('snmpLog').style.display = 'flex';
      document.getElementById('motorGraph').style.display = 'none';
      document.getElementById('maintLog').style.display = 'none';
      refreshData();
      ManageTabSelection(this);
    }
  });
});
