/* Setup login popup */
(function setupLoginPopup() {
	//if (!$('setup_menu')) {
	//return;
	//}
	var pwdtmp = "";

	var loginPopup = new Popup('login-popup');
	var html = '';
	html += '<div class="field-group clearfix" id="login-panel">';
	html += '	<div class="field-container text clearfix" style="display: none;" id="login_failure">';
	html += '		<label style="color: #AA3333;">FAILURE: Username or Password is incorrect. Please change the username or password and try again.</label>';
	html += '	</div>';
	html += '<div class="field-container text clearfix" style="display: none;" id="username_empty">';
	html += '		<label style="color: #AA3333;">FAILURE: Username field cannot be left empty. Enter the username and try again.</label>';
	html += '	</div>';
	html += '<div class="field-container text clearfix" style="display: none;" id="password_empty">';
	html += '		<label style="color: #AA3333;">FAILURE: Password field cannot be left empty. Enter the password and try again.</label>';
	html += '	</div>';
	html += '<div class="field-container text clearfix" style="display: none;" id="uname_pwd_empty">';
	html += '		<label style="color: #AA3333;">FAILURE: Username and Password field cannot be left empty. Enter the username & password and try again.</label>';
	html += '	</div>';
	html += '	<div class="field-container text clearfix" style="display: none;" id="another_session">';
	html += '		<label style="color: #AA3333;">FAILURE: Another user authenticated. Please try again later.</label>';
	html += '	</div>';
	html += '	<div class="field-container text clearfix" style="display: none;" id="password_alert">';
	html += '		<label style="color: #AA3333;">FAILURE: Password not accepted. Please try again.</label>';
	html += '	</div>';
	html += '	<div class="field-container text clearfix">';
	html += '		<label for="signin-username">User Name:</label>';
	html += '		<div id="password_container" class="iw">';
	html += '			<input value="" type="text" id="signin-username" name="username">';
	html += '		</div>';
	html += '	</div>';
	html += '	<div class="field-container text clearfix">';
	html += '		<label for="signin-password">Password:</label>';
	html += '		<div id="password_container" class="iw">';
	html += '			<input value="" type="password" id="signin-password" name="password">';
	html += '		</div>';
	html += '	</div>';
	html += '</div>';
	html += '<div class="clearfix" id="login-loading-info" style="text-align:center">';
	html += '	<img src="/img/ajax-load.gif" width="16" height="16" /> Please wait...';
	html += '</div>';

	var loginPopup2 = new Popup('login-err-popup');
	var authErrMsg = '';
	authErrMsg += ' <div id="login-error">Another user authenticated.</div>';
	authErrMsg += '	<div class="field-group clearfix">';
	authErrMsg += '		<div class="field-container text clearfix">';
	authErrMsg += '		    <label for="signin-description">Do you want to logout current user?</label>';
	authErrMsg += '	    </div>';
	authErrMsg += '	</div>';

	var confirmPopup = new Popup('login-confirmation-popup');
	var confirmMsg = '';
	confirmMsg += '	<div class="field-group clearfix">';
	confirmMsg += '		<div class="field-container text clearfix">';
	confirmMsg += '		    <label for="signin-description">You need to confirm presence at Microlok System</label>';
	confirmMsg += '	    </div>';
	confirmMsg += '	</div>';

	var confirmMsg2 = '';
	confirmMsg2 += '	<div class="field-group clearfix">';
	confirmMsg2 += '		<div class="field-container text clearfix">';
	confirmMsg2 += '		    <label for="signin-description">You need to confirm presence at Microlok System.</label>';
	confirmMsg2 += '		    <label for="signin-description">Toggle ACCEPT switch on front panel and wait for front panel to display:</label>';
	confirmMsg2 += '	    </div>';
	confirmMsg2 += '		<div class="field-container text clearfix" style="text-align: center;">';
	confirmMsg2 += '		    <label for="signin-description">CNFG</label>';
	confirmMsg2 += '		    <label for="signin-description">MODE</label>';
	confirmMsg2 += '	    </div>';
	confirmMsg2 += '	</div>';
	confirmMsg2 += '<div class="clearfix" id="login-loading-info" style="text-align:center">';
	confirmMsg2 += '	<img src="/img/ajax-load.gif" width="16" height="16" />Waiting...';
	confirmMsg2 += '</div>';

	var confirmMsgLoad = '';
	confirmMsgLoad += '	<div class="clearfix" id="login-loading-info" style="text-align:center">';
	confirmMsgLoad += '		<img src="/img/ajax-load.gif" width="16" height="16" /> Please wait...';
	confirmMsgLoad += '	</div>';

	var confirmMsgError = '';
	confirmMsgError += '<div class="field-group clearfix" id="login-panel">';
	confirmMsgError += '	<div class="field-container text clearfix" id="password_failure">';
	confirmMsgError += '		<label style="color: #AA3333;">FAILURE: Connection or server error. Refresh the page and try again.</label>';
	confirmMsgError += '	</div>';
	confirmMsgError += '</div>';

	/*function handleLogonOption() {
		if ( $('no_login_true').checked ) {
			$('password_container').className = "iw is-disabled";
			$('signin-password').className = "is-disabled";
			$('signin-password').readOnly = true;
		} else {
			$('password_container').className = "iw";
			$('signin-password').className = "";
			$('signin-password').readOnly = false;
			$('signin-password').focus();
		}
	};*/
	function checkSession(direct) {
		var result;
		new Ajax.Request('/checkSession', {
			method : 'post',
			asynchronous : false,
			onSuccess : function(transport) {
				if (transport.responseJSON != null) {
					var status = transport.responseJSON.status;
					if (status == "208") {
						console.log("sttaus=" + status);
						result = true;
						document.location = direct;
					} else if (status == "404") {
						console.log("sttaus=" + status);
						result = false;
					}
				}
			}
		});
		return result;
	}
	function doLogIn(direct) {
		checkSession(direct);
		function cancelConfirmation() {
			confirmPopup.hide();
		}

		function showPressConfirmation() {
			loginPopup.hide();
			confirmPopup.show({
				title : 'Login successful',
				message : confirmMsg,
				buttons : [ {
					title : 'Enter Configuration Mode',
					action : showWaitConfirmation
				}, {
					title : 'Cancel',
					action : cancelConfirmation
				} ]
			});
			return;
		}

		function lookForConfigStatus() {
			if ($('operation-mode').value.search('Config') >= 0) {
				document.location = "config";
			} else {
				$('board-time').fire("time:updated");
				lookForConfigStatus.delay(2);
			}
		}

		function lookForAcceptButton() {
			// TODO: AJAX for button state periodically
			new Ajax.Request('/look-accept', {
				method : 'post',
				onSuccess : function(transport) {
					/*resultErrorCode*/
					var status = transport.responseJSON.status_code;
					if (status == "0") {
						var result = transport.responseJSON.result;
						if (result == "1") {
							confirmPopup.show({
								title : 'Presence confirmation',
								message : confirmMsgLoad,
								buttons : []
							});
							$('board-time').fire("time:updated");
							lookForConfigStatus.delay(2);
						} else {
							lookForAcceptButton.delay(2);
						}
					} else {
						lookForAcceptButton.delay(2);
					}
				},
				onFailure : function() {
					lookForAcceptButton.delay(2);
				}
			});
		}

		function lookForWaitStatus() {
			if ($('operation-mode').value.search('Wait') >= 0) {
				confirmPopup.show({
					title : 'Presence confirmation',
					message : confirmMsg2,
					buttons : []
				});
				lookForAcceptButton.delay(2);
			} else {
				$('board-time').fire("time:updated");
				lookForWaitStatus.delay(2);
			}
		}

		function showWaitConfirmation() {
			confirmPopup.show({
				title : 'Presence confirmation',
				message : confirmMsgLoad,
				buttons : []
			});
			var nocache = new Date().getTime();
			new Ajax.Request('/wait-confirm', {
				method : 'post',
				parameters : {
					'password' : pwdtmp,
					'nocache' : nocache
				},
				onSuccess : function(transport) {
					var status = transport.responseJSON.status_code;
					if (status == "0") {
						$('board-time').fire("time:updated");
						lookForWaitStatus.delay(2);
					} else {
						confirmPopup.show({
							title : 'FAILURE:',
							message : confirmMsgError,
							buttons : [ {
								title : 'Close'
							} ]
						});
					}
				},
				onFailure : function() {
					confirmPopup.show({
						title : 'FAILURE:',
						message : confirmMsgError,
						buttons : [ {
							title : 'Close'
						} ]
					});
				}
			});
		}

		function loginWait(toggle) {
			$('signin-form')[toggle ? 'hide' : 'show']();
			$('login-loading-info')[toggle ? 'show' : 'hide']();
			$$('#login-popup .button').invoke(toggle ? 'hide' : 'show');
			Popup.frobIEElements();
		}


		var loginAttempts = 0;
		function submitLoginForm() {
			if (emptyCheck($('signin-username'))
					&& emptyCheck($('signin-password'))) {
				$('signin-username').focus();
				$('uname_pwd_empty').show();
				$('username_empty').hide();
				$('password_empty').hide();
				return;
			} else if (emptyCheck($('signin-username'))) {
				$('uname_pwd_empty').hide();
				$('username_empty').show();
				$('password_empty').hide();
				return;
			} else if (emptyCheck($('signin-password'))) {
				$('uname_pwd_empty').hide();
				$('username_empty').hide();
				$('password_empty').show();
				return;
			}
			/*var mode = $('no_login_true').checked?'view':'edit';*/
			$('login-popup').getElementsByClassName('popup-buttons')[0].hide();
			$('login-panel').hide();
			$('login-loading-info').show();
			var pwd = document.getElementById('signin-password').value;
			var uname = document.getElementById('signin-username').value;
			var nocache = new Date().getTime();
			var data = {};
			data.username = uname;
			data.password = pwd;

			new Ajax.Request('/adminAuth', {
				method : 'post',
				withCredentials : true,
				parameters : {
					'password' : pwd,
					'username' : uname,
					'nocache' : nocache
				},
				onSuccess : function(transport) {
					//debugger;
					//document.location = "deviceConfiguration";
					console.log("response=" + JSON.stringify(transport));
					if (transport.responseJSON != null) {
						var status = transport.responseJSON.status;
						if (status == "200") {
							document.location = direct;
						} else if (status == "100") {
							pwdtmp = temp_pwd;
							if (mode == "view") {
								document.location = "deviceConfiguration";
							} else {
								//loginPopup.hide();
								//showPressConfirmation();
								// $('#operation-mode input').val("fgg");
								//$('input[id="operation-mode"]').val('some value');
								//$('operation-mode').val("Reset-Wait");
								showWaitConfirmation();
							}
						} else if (status == "404") {
							//alert();
							$('login-popup').getElementsByClassName(
									'popup-buttons')[0].show();
							$('login-panel').show();
							$('login-loading-info').hide();
							$('login_failure').show();
							exit;
						} else {
							$('login-panel').show();
							$('login-loading-info').hide();
							//	$('another_session').hide();
							//	$('password_alert').hide();
							$('login_failure').show();
							$('login-popup').getElementsByClassName(
									'popup-buttons')[0].show();
						}
					} else {
						$('another_session').hide();
						$('password_alert').hide();
						$('login_failure').show();
					}
				},
				onFailure : function() {
					//debugger;
					$('login-panel').show();
					$('login-loading-info').hide();
					$('another_session').hide();
					$('password_alert').hide();
					$('login_failure').show();
				}
			});
			return;
		}

		function emptyCheck(el) {
			if ($F(el) === '') {
				el.focus();
				return true;
			} else {
				return false;
			}
		}
		loginPopup.show({
			title : 'Sign In',
			message : html,
			buttons : [ {
				title : 'Cancel'
			}, {
				title : 'Sign In',
				action : submitLoginForm
			} ]
		});

		$('signin-password').observe('keypress', function(evt) {
			if (evt.keyCode == 13) {
				submitLoginForm();
			}
		});

		$('signin-username').observe('keypress', function(evt) {
			if (evt.keyCode == 13) {
				submitLoginForm();
			}
		});


		$('signin-username').focus();
		$('signin-password').observe('focus', function(event) {
			if (emptyCheck($('signin-username'))) {
				$('username_empty').show();
				$('password_empty').hide();
				$('uname_pwd_empty').hide();
			}
		});
		$('login-loading-info').hide();
		$('login_failure').hide();
		$('another_session').hide();
		$('password_alert').hide();
		$('login-popup').getElementsByClassName('popup-buttons')[0].show();
		Popup.frobIEElements();
	}
	$('configuration_menu').observe('click', function() {
		if($('watcher') != null){
			if(!checkSession('deviceConfiguration'))
			doLogIn('deviceConfiguration');
		}else{		
			doLogIn('deviceConfiguration');
		}
	});
	if($('login') != null){
	$('login').observe('click', function() {
		doLogIn('/');
	});}
	else if($('logout') != null){
	$('logout').observe(
			'click',
			function() {
				new Ajax.Request('/endSession', {
					method : 'post',
					asynchronous : false,
					onSuccess : function(transport) {
						//debugger;
						console.log("checkSession response="
								+ JSON.stringify(transport));
						if (transport.responseJSON != null) {
							var status = transport.responseJSON.status;
							if (status == "200") {
								console.log("staus=" + status);
								document.location = '/';
							} else if (status == "404") {
								console.log("staus=" + status);
								document.location = '/';
							}
						}
					}
				});
			});
	}
})();