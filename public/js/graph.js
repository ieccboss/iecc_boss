var $j = jQuery;
$j(function() {
	var x2js = new X2JS();
	//var rec;
	var pageSize=25;
	var pageNum=1;
	var socket = io();
	var options = {
		lines : {
			show : true
		},
		points : {
			show : true
		},grid: {
			hoverable: true,
			clickable: true
		},
		xaxis : {
			tickDecimals : 2,
			tickSize : 1
		},
		yaxis : {
			tickDecimals : 1,
			tickSize : 2
		},
		
	};

	var latestGraphFile;
	var device;
	var currlatchOutStatus;

	var data = [];
	var selected = [];

	$j.plot("#placeholder", data, options);

	var alreadyFetched = {};

	function displayGraph() {
		function onDataReceived(series) {	
			var custData = [];
			var tmpArr = [];
				for (i = 0;i<series.length;i++) {
				    var a = [];
				    for (n in series[i]) {
				        a[n] = series[i][n];
				        tmpArr.push(a[n]);
				    }
				    custData[i]=tmpArr;
				    tmpArr=[];
				}
				gSeries={"label":latestGraphFile,"data":custData};
			//	alert(JSON.stringify(gSeries));
			var firstcoordinate = "(" + gSeries.data[0][0] + ", " + gSeries.data[0][1] + ")";
		//	button.siblings("span").text("Fetched " + series.label + ", first point: "	+ firstcoordinate);

			if (!alreadyFetched[gSeries.label]) {
				alreadyFetched[gSeries.label] = true;
				data.push(gSeries);
			}
			window.onresize = function(event) {
				$j.plot("#placeholder", data, options);
			}
			$j("#motorGraph").css("display", "flex");
			$j("#motorGraph").css("flex", "1");
			$j("#motorGraph").css("flex-direction", "column");
			$j("#instData").css("display", "none");
			$j("#eventLog").css("display", "none");
			$j("#maintLog").css("display", "none");
			$j("#snmpLog").css("display", "none");
			$j("#swop").css("display", "none");
			//$j("#motor_current").css("display", "inline");
			$j("#specific_graph").css("display", "inline");
			$j("#graphDatePicker").css("display", "inline");
			$j.plot("#placeholder", data, options);
		}

		$j.ajax({
			url : device+'/'+latestGraphFile+'.json',
			type : "GET",
			dataType : "json",
			success : onDataReceived
		});
	}

	$j("#graphDatePicker").click(function(e) {
		   e.stopPropagation();
	});
		
	$j('#graphDatePicker').datepicker({
		//alert('dtPickr');
		language : 'en',
		autoClose : 'true',
		dateFormat : "yyyy-mm-dd",
		position : 'top left',
		onSelect : function onSelect(fd, date) {
			 //alert(fd);
			var data = {
				device : device,
				date : fd,
				latestGraphFile : latestGraphFile
			};
			$j.ajax({
				url : "specificLog",
				type : "POST",
				data : data,
				dataType : "json",
				success : getCurrentFiles
			});
		}
	})

	function getCurrentFiles(result) {
		$j('#graphFiles').find('option').remove();
	//	$j('#current-files').find('ul').remove();
		$j('#graphFiles')
				.append(
						'<option selected="" value="SelectCurrentFile">Select Current File</option>');
		for (var i = 0; i < result.currentFiles.length; i++) {
			$j('#graphFiles').append('<option value="' + result.currentFiles[i].FILE_NAME + '">'
							+ result.currentFiles[i].FILE_NAME + '</option>');

//			$j('#current-files').append("<ul><li><input type='checkbox' name='"+result.currentFiles[i].FILE_NAME+"'/>"+result.currentFiles[i].FILE_NAME+"</li></ul>");
		}
	}

	function displayBaseLine(){
		function onBaseline(series) {	
			var custData = [];
			var tmpArr = [];
				for (i = 0;i<series.length;i++) {
				    var a = [];
				    for (n in series[i]) {
				        a[n] = series[i][n];
				        tmpArr.push(a[n]);
				    }
				    custData[i]=tmpArr;
				    tmpArr=[];
				}
				gSeries={"label":'BaseLine',"data":custData};
			var firstcoordinate = "(" + gSeries.data[0][0] + ", " + gSeries.data[0][1] + ")";
		//	button.siblings("span").text("Fetched " + series.label + ", first point: "	+ firstcoordinate);
			
			for (var i = 0; i < data.length; i++) {
				   console.log(data[i].label);
				   if(data[i].label == 'BaseLine'){
				     data.splice(i, 1);
				   }
				}
			data.push(gSeries);
			/*if (!alreadyFetched[gSeries.label]) {
				alreadyFetched[gSeries.label] = true;
				data.push(gSeries);
			}*/
			window.onresize = function(event) {
				$j.plot("#placeholder", data, options);
			}
			$j("#motorGraph").css("display", "flex");
			$j("#motorGraph").css("flex", "1");
			$j("#motorGraph").css("flex-direction", "column");
			$j("#instData").css("display", "none");
			$j("#eventLog").css("display", "none");
			$j("#maintLog").css("display", "none");
			$j("#snmpLog").css("display", "none");
			$j("#swop").css("display", "none");
			//$j("#motor_current").css("display", "inline");
			$j("#specific_graph").css("display", "inline");
			$j("#graphDatePicker").css("display", "inline");
			$j.plot("#placeholder", data, options);
		}

		$j.ajax({
			url : device+'/'+latestGraphFile+'.json',
			type : "GET",
			dataType : "json",
			success : onBaseline
		});
		}
	
	function getData(result) {
		if (result.status == '200') {
			latestGraphFile = result.file;
			displayBaseLine();
			$j("#motor_current").css("display", "inline");
			$j("#specific_graph").css("display", "inline");
			$j("#graphDatePicker").css("display", "inline");
			$j("#graphFiles").css("display", "inline");
		//	$j('#graphFiles').find('option').remove();
			$j("#swopLog").css("display", "none");
			$j("#snmpLog").css("display", "none");
			$j("#snmpLog").css("display", "none");
			$j("#instData").css("display", "none");
			$j("#maintLog").css("display", "none");
			$j("#instData").css("display", "none");
			$j("#eventLog").css("display", "none");
			
			$j("#graphFiles").val(latestGraphFile);
			
		//	$j('#graphFiles').append('<option value="' + result.file + '">' + result.file + '</option>');
			
		//	$j('#current-files').find('ul').remove();
		//	$j('#current-files').append("<ul><li><input type='checkbox' name='"+result.file+"'/>"+result.file+"</li></ul>");
			
		} //else if (result.status == '404') {
		else {
			$j("#motorGraph").css("display", "flex");
			$j("#motorGraph").css("flex", "1");
			$j("#motorGraph").css("flex-direction", "column");
			$j("#swopLog").css("display", "none");
			$j("#snmpLog").css("display", "none");
			$j("#instData").css("display", "none");
			$j("#maintLog").css("display", "none");
			$j("#instData").css("display", "none");
			$j("#eventLog").css("display", "none");
			document.getElementById('placeholder').innerHTML = "<pre style='align:center'><strong>No Motor Current File Available For This Device, Please " +
					"Choose Another Device<strong></pre>";
		}
	}
	function baseLineFile(result) {
		if (result.status == '200') {
	//	$j('#baseline-file').find('ul').remove();
	//	$j('#baseline-file').append("<ul><li><input type='checkbox' name='"+result.file+"'/>"+result.file+"</li></ul>");
			$j("#graphFiles").css("display", "inline");
			$j('#graphFiles').find('option').remove();
			$j('#current-files').find('ul').remove();
			for (var i = 0; i < result.currentFiles.length; i++) {
			$j('#graphFiles').append(
					'<option value="' + result.currentFiles[i].FILE_NAME + '">' + result.currentFiles[i].FILE_NAME + '</option>');
			$j('#current-files').append("<ul><li><input type='checkbox' class='sm' id='"+result.currentFiles[i].FILE_NAME+"'/>"+result.currentFiles[i].FILE_NAME+"</li></ul>");
			}
			
		} else{
			document.getElementById('placeholder').innerHTML = "<pre style='align:center'><strong>No Motor Current File Available For This Device, Please " +
			"Choose Another Device<strong></pre>";
		}
		var deviceData = {
				devId : device
			};
		$j.ajax({
			url : "latestLog",
			type : "POST",
			data : deviceData,
			dataType : "json",
			success : getData
		});
	}
	


	$j(".dropdown-content").on("click", "ul", function(e){
			if(document.getElementById($j(this).text()).checked){
			if(selected.indexOf($j(this).text()) == -1){ 
				selected.push($j(this).text());
				 latestGraphFile = $j(this).text();
				   //if(gSeries.label != latestGraphFile)
				 if(selected.indexOf($j(this).text()) != -1){
					 alreadyFetched[$j(this).text()] = false;
						displayGraph();
				 }
				}
			} else{
				selected.splice(selected.indexOf($j(this).text()), 1);
				for (var i = 0; i < data.length; i++) {
					   if(data[i].label == ($j(this).text())){
					     data.splice(i, 1);
					   }
					}
				displayGraph();
			}
		   e.stopPropagation();
		});
	
	$j("#graphFiles").click(function(e) {
		latestGraphFile = ($j("#graphFiles").val());
		displayBaseLine();			
		e.stopPropagation();
	});

	
	
	$j("#motor_current").click(function(e) {
		$j("#graphDatePicker").val("Date: MM/DD/YYYY");
		if (device) {
			data = [];
		//if (true) {
		ManageTabSelection(this);
			var deviceData = {
				devId : device
			};
			alreadyFetched = {};
			$j.ajax({
				url : "currentFiles",
				type : "POST",
				data : deviceData,
				dataType : "json",
				success : baseLineFile
			});
		} else {
			ShowAlert('Please Select a Device First');
		}
		$j("#current-files").css("display", "none");
		e.stopPropagation();
	});
	
	$j("#select-current-file").click(function(e) { 
		$j("#current-files").css("display", "block");
		$j("#baseline-file").css("display", "none");
		e.stopPropagation();
	});
	
	$j("#clear-all").click(function(e) {
		$j("#current-files").css("display", "none");
		data = [];
		for (var i = 0; i < selected.length; i++) {
			document.getElementById(selected[i]).checked = false;
		}
		$j("#graphFiles").click();
		e.stopPropagation();
	});
	
	$j(document).click(function (e) {            
	    $j('#current-files:visible').hide();
	});
		
	$j("#horizontalSplittr").click(function() {
		$j.plot("#placeholder", data, options);
	});

	$j("#verticalSplittr").click(function() {
		$j.plot("#placeholder", data, options);
	});

	// -- instantaneous data
	$j("#device_data").click(function() {
		$j(this).showInstData({
			param1 : device,
			param2 : $j("#lbl-device-ip").text(),
			//param3 : currlatchOutStatus
		});
	});

	$j.fn.showInstData = function(ev) {
	//	alert('--ev-1-deviceID='+ev.param1+'--ev-2-ip='+ev.param2);
		if (ev.param1 && ev.param2) {
		ManageTabSelection($j("#device_data"));
			$j("#eventLog").css("display", "none");
			$j("#swopLog").css("display", "none");
			$j("#snmpLog").css("display", "none");
			$j("#motorGraph").css("display", "none");
			$j("#instData").css("display", "flex");
			$j("#specific_graph").css("display", "none");
			$j("#maintLog").css("display", "none");
			var selectedIp = ev.param2;
			device = ev.param1;
			currlatchOutStatus = ev.param3;
			//var instDataSocket;
			/*if (selectedIp) {
				instDataSocket = io.connect(selectedIp);
				instDataSocket.on('message', function(message) {
					console.log(message + ' with ' + selectedIp);
				});
			}
			selectedIp = selectedIp.substr(selectedIp.lastIndexOf('/') + 1,
					selectedIp.lastIndexOf(':') - selectedIp.lastIndexOf('/')
							- 1);
			if (selectedIp) {*/
				socket.emit('getInstData', device);
				socket.on('getInstData',
								function(message) {
									$j("#latchOutStatus")
											.text(
													message[0].latch_out_config_setting);
									$j("#posSetting")
											.text(
													message[0].position_setting);
									$j("#ipdMode").text(
											message[0].ipd_mode);
									$j("#ieccPartNum")
											.text(
													message[0].part_number);
									$j("#ieccSerNum")
											.text(
													message[0].serial_number);
									$j("#sysSoftwareVer")
											.text(
													message[0].system_software_version);
									$j("#commProVer")
											.text(
													message[0].comm_processor_version);
									$j("#vitalFpgaAltVer")
											.text(
													message[0].vital_fpga_altera_version);
									$j("#vitalFpgaXilVer")
											.text(
													message[0].vital_fpga_xilinx_version);
									$j("#sysHardwareVer")
											.text(
													message[0].system_hardware_version);
									$j("#switchPartNum")
											.text(
													message[0].switch_mac_part_num);
									$j("#switchMacSerNum")
											.text(
													message[0].switch_mac_serial_num);
									$j("#switchMacType")
											.text(
													message[0].switch_mac_type);
									$j("#switchMacGearRatio")
											.text(
													message[0].switch_mac_gear_ratio);
									$j("#switchMacDirectionIns")
											.text(
													message[0].switch_mac_direction_installation);
									$j("#switchMacMotorCont")
											.text(
													message[0].switch_mac_motor_controller);
									$j("#switchMacMotorV")
											.text(
													message[0].switch_mac_motor_v);
									$j("#milePost").text(
											message[0].mile_post);
									$j("#chainingMode").text(
											message[0].chaining_mode);
									$j("#nominalPos")
											.text(
													message[0].nominal_position_gps);
									$j("#ieccManufacturingDt")
											.text(
													message[0].manufacturing_date);
									$j("#switchMacManufacturingShipDt")
											.text(
													message[0].switch_mac_manufacturing_shipping_date);
									$j("#switchMacInsDt")
											.text(
													message[0].switch_mac_installation_date);
						});

		} else {
			ShowAlert('Please Select Insta Device First');
		}
	};
	
	// Events Log

	$j("#pageSize").change(function(e) {
		//alert($j("#pageSize").val());
		e.stopPropagation();
		if($j("#pageSize" ).val()>100){
			ShowAlert ("Items per page should be integer value in range 0 to 100");
			$j("#pageSize").val(pageSize);
			return false;
		}
		if($j('#pageSize').val() > Number($j('#totalPageNum').text())){
			//alert(pageNum);
			ShowAlert ("Maximum Value of Records Per Page : "+Number($j('#totalPageNum').text()));
			$j("#pageSize").val(pageSize);
			//return;
		}
		/*	var start = $j("#pageNum" ).val()*$j("#pageSize" ).val()-($j("#pageSize" ).val());
		showRecord('#eventDataTbl',start,($j("#pageNum" ).val()*$j("#pageSize" ).val())-1);*/
		//alert('pgSiz');
		$j("#event_log").click();
	  });
	
	$j("#pageSize").click(function(e) {
		pageSize = $j("#pageSize").val();
	});
	
	$j("#pageNum").click(function(e) {
		pageNum = $j("#pageNum").val();
	});
	
	$j("#pageNum").change(function(e) {
		e.stopPropagation();
	//	var start = $j("#pageNum" ).val()*$j("#pageSize" ).val()-($j("#pageSize" ).val());
	//	showRecord('#eventDataTbl',start,($j("#pageNum").val()*$j("#pageSize" ).val())-1);
		if($j('#pageNum').val() == Math.ceil($j('#totalPageNum').text()/($j("#pageSize" ).val()))){
			disableClick("#eventLogLast","#eventLogNext");
		}
		if($j('#pageNum').val() > Math.ceil($j('#totalPageNum').text()/($j("#pageSize" ).val()))){
			//alert(pageNum);
			ShowAlert ("Maximum Value of Page : "+Math.ceil($j('#totalPageNum').text()/($j("#pageSize" ).val())));
			$j("#pageNum").val(pageNum);
			//return;
		}
	//alert('pgNum');
	$j("#event_log").click();
	});

	$j("#eventLogNext").click(function(e) {
		//alert($j('#pageNum').val());
		e.stopPropagation();
		$j('#pageNum').val(Number($j('#pageNum').val())+1);
		$j("#pageNum").change();
		if($j('#pageNum').val() == Math.ceil($j('#totalPageNum').text()/($j("#pageSize" ).val()))){
			disableClick("#eventLogLast","#eventLogNext");
		}
		enableClick("#eventLogPrev","#eventLogFirst");
	});
	
	$j("#eventLogLast").click(function(e) {
		$j('#pageNum').val(Math.ceil($j('#totalPageNum').text()/($j("#pageSize" ).val())));
		$j("#pageNum").change();
		disableClick("#eventLogLast","#eventLogNext");
		enableClick("#eventLogPrev","#eventLogFirst");
	});
	
	$j("#eventLogPrev").click(function(e) {
		e.stopPropagation();
		if(Number($j('#pageNum').val())-1 == 0){
			return;
		}
		$j('#pageNum').val(Number($j('#pageNum').val())-1);
		//alert($j('#pageNum').val());
		$j("#pageNum").change();
		if($j('#pageNum').val() == 1){
			disableClick("#eventLogPrev","#eventLogFirst");
		}
		enableClick("#eventLogLast","#eventLogNext");
	});
	
	$j("#eventLogFirst").click(function(e) {
		if(Number($j('#pageNum').val())-1 == 0){
			return;
		}
		$j('#pageNum').val(1);
		$j("#pageNum").change();
		disableClick("#eventLogPrev","#eventLogFirst");
		enableClick("#eventLogLast","#eventLogNext");
	});
	
	function disableClick(el1,el2){
		$j(el1).css("pointer-events", "none");
		$j(el2).css("pointer-events", "none");
	}
	
	function enableClick(el1,el2){
		$j(el1).css("pointer-events", "auto");
		$j(el2).css("pointer-events", "auto");
	}
	
	$j("#specific_event_log").click(function(e) {
		e.stopPropagation();
	});


	 function showRecord(el1,start,end,rec){
		 $j(el1).find('tr').remove();
		 var tdClass ='width:10%';
		 var row = null;
		 $j.each(rec, function(outerKey, list) {
		 if(outerKey >= start){
		if ((outerKey-start) % 2 === 0) {
		row = $j('<tr style="">', {
		id : 'row_' + outerKey
		});
		} else {
		row = $j('<tr style="background: #D7DCE1;">', {
		id : 'row_' + outerKey
		});
		}
		$j.each(list, function(innerKey, value) {
		if(innerKey=='description'){
		tdClass='padding: 8px;border: 2px solid #D7DCE1;width: 37%;';
		}
		else if(innerKey=='INSPECTION_DESCRIPTION' || innerKey=='INSPECTION_PERSONNEL'){
		tdClass='padding: 8px;border: 2px solid #D7DCE1;width: 285px;';
		}
		else if(innerKey=='SWITCH_MACHINE_MAINTENANCE_NUM' || innerKey=='LOG_DATE' || innerKey=='INSPECTION_START_TIME'|| innerKey=='INSPECTION_END_TIME' || innerKey=='INSPECTION_DURATION'){
		tdClass='padding: 8px;border: 2px solid #D7DCE1;width: 135px';
		}
		else if( innerKey == 'TIME_SINCE_LAST_INSPECTION'){
		tdClass='padding: 8px;border: 2px solid #D7DCE1;width: 120px;';
		}
		else{
		tdClass='padding: 8px;border: 2px solid #D7DCE1;width:9%;';
		}
		var col = $j('<td >', {

		style : tdClass,
		//id : value,
		text : value
		})
		row.append(col);
		});
		$j(el1).append(row);
		if(outerKey==end){
		return false;
		}
		}
		 });
		 }
	 
	
	$j("#event_log").click(function(e) {
		$j("#noDataEvtLog").css("display", "none");
		if($j('#pageSize').val()){	
			pageSize = $j('#pageSize').val();
		}else{
			pageSize = 25;
		}
		if($j('#pageNum').val()){	
			pageNum = $j('#pageNum').val()-1;
		}else{
			pageNum = 0;
		}
		
		if (device) {
		socket.emit('getEventLog',device,pageSize,pageNum);
		socket.on('eventLog',function(message,tot){
			processRec('#eventDataTbl',"#totalPageNum","#evtLogdataPresent","#noDataEvtLog",message,tot);
		});

		 ManageTabSelection(this);	
		 	$j("#swopLog").css("display",'none');
		 	$j("#snmpLog").css("display", "none");
		 	$j("#eventLog").css("display", "flex");
		 	$j("#eventLog").css("flex", "1");
		 	$j("#eventLog").css("flex-direction", "column");
		 	$j("#instData").css("display", "none");
		 	$j("#motorGraph").css("display", "none");
		 	$j("#specific_graph").css("display", "none");
		 	$j("#maintLog").css("display", "none");
		} else {
			ShowAlert('Please Select a Device First');
		}
	});
	
	function processRec(el1,el2,el3,el4,res,tot){
		 if(res != null && !$j.isEmptyObject(res) && res != ''){
			 $j(el1).find('tr').remove();
        	$j(el3).css("display", "flex");
 		 	$j(el3).css("flex", "1");
 		 	$j(el3).css("flex-direction", "row");
			 var record = res;	
		$j(el2).text(tot);
			showRecord(el1,0,pageSize,res);
         } else {
        	 $j(el3).css("display", "none");
        	 $j(el4).css("display", "block");
        	 $j(el2).text(tot);
        	 showRecord(el1,0,pageSize,res);
        	// $j(el1).parent().parent().parent().parent().parent().parent().parent().html("<div id='sm'><pre style='align:center'><strong>No Logs Available For This Device, Please "
 				//	+ "Choose Another Device<strong></pre><div>");
		}
	};
	
	// -- maintenance log
	
	$j("#maintenance_log").click(function() {
		$j("#noDataMaintLog").css("display", "none");
		if($j('#maintPageSize').val()){	
			pageSize = $j('#maintPageSize').val();
		}else{
			pageSize = 25;
		}
		if($j('#maintPageNum').val()){	
			pageNum = $j('#maintPageNum').val()-1;
		}else{
			pageNum = 1;
		}
		
		if (device) {
		socket.emit('getMaintenanceLog',device,pageSize,pageNum);
		socket.on('maintenanceLog',function(message,tot){
				processRec('#maintDataTbl','#maintTotPageNum',"#maintLogdataPresent","#noDataMaintLog",message,tot);
			});
			
		ManageTabSelection(this);
		$j("#maintLog").css("display", "flex");
		$j("#maintLog").css("flex", "1");
		$j("#maintLog").css("flex-direction", "column");
		$j("#eventLog").css("display", "none");
		$j("#swopLog").css("display", "none");
		$j("#snmpLog").css("display", "none");
		$j("#motorGraph").css("display", "none");
		$j("#instData").css("display", "none");
		$j("#specific_graph").css("display", "none");
	//	document.getElementById('maintLog').innerHTML = "<pre style='align:center'><strong>Maintenance Log Functionality Not Available<strong></pre>";
		
		} else {
			ShowAlert('Please Select a Device First');
		}
	});
	
	$j("#maintPageSize").change(function(e) {
		e.stopPropagation();
		if($j("#maintPageSize" ).val()>100){
			ShowAlert ("Items per page should be integer value in range 0 to 100");
			$j("#maintPageSize").val(pageSize);
			return false;
		}
		/*var start = $j("#maintPageNum" ).val()*$j("#maintPageSize" ).val()-($j("#maintPageSize" ).val());
		//alert(start);
		showRecord('#maintDataTbl',start,($j("#maintPageNum" ).val()*$j("#maintPageSize" ).val())-1);*/
		if($j('#maintPageSize').val() > Number($j('#maintTotPageNum').text())){
			//alert(pageNum);
			ShowAlert ("Maximum Value of Records Per Page : "+Number($j('#maintTotPageNum').text()));
			$j("#maintPageSize").val(pageSize);
			//return;
		}
		$j("#maintenance_log").click();
	  });
	
	$j("#maintPageSize").click(function(e) {
		pageSize = $j("#maintPageSize").val();
	});
	
	$j("#maintPageNum").change(function(e) {
		e.stopPropagation();
		/*var start = $j("#maintPageNum" ).val()*$j("#maintPageSize" ).val()-($j("#maintPageSize" ).val());
		//alert(start);
		showRecord('#maintDataTbl',start,($j("#maintPageNum").val()*$j("#maintPageSize" ).val())-1);*/
		if($j('#maintPageNum').val() == Math.ceil($j("#maintPageSize" ).val()/($j("#maintPageSize" ).val()))){
			disableClick("#maintLogLast","#maintLogNext");
		}
		if($j('#maintPageNum').val() > Math.ceil($j('#maintTotPageNum').text()/($j("#maintPageSize" ).val()))){
			//alert(pageNum);
			ShowAlert ("Maximum Value of Page : "+Math.ceil($j('#maintTotPageNum').text()/($j("#maintPageSize" ).val())));
			$j("#maintPageNum").val(pageNum);
			//return;
		}
		$j("#maintenance_log").click();
	  });
	
	$j("#maintPageNum").click(function(e) {
		pageNum = $j("#maintPageNum").val();
	});

	$j("#maintLogNext").click(function(e) {
		$j('#maintPageNum').val(Number($j('#maintPageNum').val())+1);
		$j("#maintPageNum").change();
		if($j('#maintPageNum').val() == Math.ceil($j('#maintTotPageNum').text()/($j("#maintPageSize" ).val()))){
			disableClick("#maintLogLast","#maintLogNext");
		}
		enableClick("#maintLogPrev","#maintLogFirst");
	});
	
	$j("#maintLogLast").click(function(e) {
		$j('#maintPageNum').val(Math.ceil($j('#maintTotPageNum').text()/($j("#maintPageSize" ).val())));
	//	alert($j('#maintPageNum').val());
		$j("#maintPageNum").change();
		disableClick("#maintLogLast","#maintLogNext");
		enableClick("#maintLogPrev","#maintLogFirst");
	});
	
	$j("#maintLogPrev").click(function(e) {
		if(Number($j('#maintPageNum').val())-1 == 0){
			return;
		}
		$j('#maintPageNum').val(Number($j('#maintPageNum').val())-1);
		$j("#maintPageNum").change();
		if($j('#maintPageNum').val() == 1){
			disableClick("#maintLogPrev","#maintLogFirst");
		}
		enableClick("#maintLogLast","#maintLogNext");
	});
	
	$j("#maintLogFirst").click(function(e) {
		if(Number($j('#maintPageNum').val())-1 == 0){
			return;
		}
		$j('#maintPageNum').val(1);
		$j("#maintPageNum").change();
		disableClick("#maintLogPrev","#maintLogFirst");
		enableClick("#maintLogLast","#maintLogNext");
	});
	
	$j("#specific_maintenance_log").click(function(e) {
		e.stopPropagation();
	});
	
	$j("#motor_current_data_new").click(function(e) {
		ManageTabSelection(this);
		$j("#motorGraphNew").css("display", "flex");
		$j("#motorGraphNew").css("flex", "1");
		$j("#maintLog").css("flex-direction", "column");
		$j("#eventLog").css("display", "none");
		$j("#swopLog").css("display", "none");
		$j("#motorGraph").css("display", "none");
		$j("#instData").css("display", "none");
		$j("#specific_graph").css("display", "none");
		$j("#maintLog").css("display", "none");
	});
	
	
	
	$j("<div id='tooltip'></div>").css({
		position: "absolute",
		display: "none",
		border: "1px solid #fdd",
		padding: "2px",
		"background-color": "#fee",
		opacity: 0.80
	}).appendTo("body");

	$j("#placeholder").bind("plothover", function (event, pos, item) {

		if ($j("#enablePosition:checked").length > 0) {
			var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
			$j("#hoverdata").text(str);
		}

		if ($j("#enableTooltip:checked").length > 0) {
			if (item) {
				var x = item.datapoint[0].toFixed(2),
					y = item.datapoint[1].toFixed(2);

				$j("#tooltip").html(item.series.label + " of " + x + " = " + y)
					.css({top: item.pageY+5, left: item.pageX+5})
					.fadeIn(200);
			} else {
				$j("#tooltip").hide();
			}
		}
	});

	$j("#placeholder").bind("plotclick", function (event, pos, item) {
		if (item) {
			$j("#clickdata").text(" - click point " + item.dataIndex + " in " + item.series.label);
			plot.highlight(item.series, item.datapoint);
		}
	});

	// Add the Flot version string to the footer

	//$j("#footer").prepend("Flot " + $j.plot.version + " &ndash; ");
	
	
	
	
	$j(".dropdown dt a").on('click', function() {
		  $j(".dropdown dd ul").slideToggle('fast');
		});

		$j(".dropdown dd ul li a").on('click', function() {
		  $j(".dropdown dd ul").hide();
		});

		function getSelectedValue(id) {
		  return $j("#" + id).find("dt a span.value").html();
		}

		$j(document).bind('click', function(e) {
		  var $jclicked = $j(e.target);
		  if (!$jclicked.parents().hasClass("dropdown")) $j(".dropdown dd ul").hide();
		});

		$j('.mutliSelect input[type="checkbox"]').on('click', function() {

		  var title = $j(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
		    title = $j(this).val() + ",";

		  if ($j(this).is(':checked')) {
		    var html = '<span title="' + title + '">' + title + '</span>';
		    $j('.multiSel').append(html);
		    $j(".hida").hide();
		  } else {
		    $j('span[title="' + title + '"]').remove();
		    var ret = $j(".hida");
		    $j('.dropdown dt a').append(ret);

		  }
		});
});

function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // Edge (IE 12+) => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

function watch(maxTime){
	if (detectIE()) {
		var eventSess = document.createEvent("MouseEvent");
		eventSess.initMouseEvent("click",true,true,window,0,0,0,0,0,false,false,false,false,0,null);
	}else{
	var eventSess = new MouseEvent('click', {
	    view: window,
	    bubbles: true,
	    cancelable: true
	  });
	}
	var counter = maxTime;
	var temp;
	  setInterval(function() {
	    counter--;
	    
	    document.onload = resetCounter;
	//    document.onmousemove = resetCounter;
	    document.onmousedown = resetCounter;
	  //  document.ontouchstart = resetCounter;
	    document.onclick = resetCounter;
	    document.onscroll = resetCounter;
	    document.onkeypress = resetCounter;
	    
	    function resetCounter() {
	    //	if(counter === 5){
	    		var xhttp = new XMLHttpRequest();
	    		xhttp.onreadystatechange = function() {
	    			if (this.readyState == 4 && this.status == 200) {
	    			// alert(this.responseText);
	    			if(this.responseText == 200)
	    			{
	    				counter = maxTime;
	    				temp = false;
	    		}
	    	}
	  };
	  xhttp.open("POST", "renewSession", true);
	  xhttp.send();  	
	        //}
	    	clearInterval(counter);
	        counter = maxTime; 
	    }
	    if (counter === 30) {
	    	ShowSessionConfirm('Your session will expire in 30 seconds. Do you want to extend the session?', function(){	 
	    		if(!temp){
	    			temp = true;
	    			 messagePopup.hide();
	    		} else {
	    			temp = false;
	    			messagePopup.hide();
	    			ShowAlert('Your session is expired, please login again',function(){
	    			document.getElementById('logout').dispatchEvent(eventSess);
	    			});
	    		}counter = maxTime; temp = false;
	    		var xhttp = new XMLHttpRequest();
	    		  xhttp.onreadystatechange = function() {
	    		    if (this.readyState == 4 && this.status == 200) {
	    		    // alert(this.responseText);
	    		     if(this.responseText == 200)
	    		     {
	    		    	 counter = maxTime;
	    		    	 temp = false;
	    		     }
	    		    }
	    		  };
	    		  xhttp.open("POST", "renewSession", true);
	    		  xhttp.send();
			},function(){
				document.getElementById('logout').dispatchEvent(eventSess);
				messagePopup.hide();
			});
	    }    
	     if (counter === 0) { 
	    	 temp = true;
	        clearInterval(counter);
	        counter = maxTime;
	    } 

	  }, 1000);
}
