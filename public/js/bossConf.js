var $j = jQuery;
var objConfigurationScreen = {};
var SNMPLogMaxSize;
var AllLogsMaxZize;
$j(function() {
	
	if (detectIE()) {
		//alert('IE detected');
		var event = document.createEvent("MouseEvent");
		event.initMouseEvent("click",true,true,window,0,0,0,0,0,false,false,false,false,0,null);
	}else{
		var event = new MouseEvent('click', {
		    view: window,
		    bubbles: true,
		    cancelable: true
		  });
	}
	
	toggleEnableDisable(true);
	toggleEnableDisableDeleteLogsTab(true);
	document.getElementById("to_date").readOnly = true;
	document.getElementById("from_date").readOnly = true;
	var socket = io();
	getConfigurationData();
	$j("#editBossConf").click(function(e) {
		$j.ajax({
			url : "checkSession",
			type : "POST",
			dataType : "json",
			success : editBossConf
		});
	});

	function editBossConf(result) {
		if(result.status == 208){
		$j("#editBossConf").css("display", "none");
		$j("#cancelEditBossConf").css("display", "block");
		$j("#saveBossConf").css("display", "block");
		$j("#ipInputBox").css("display", "block");
		$j("#ipInputBoxDisabled").css("display", "none");
		toggleEnableDisable(false);
		}else{
			document.getElementById('configuration_menu').dispatchEvent(event);
		}
	}

	$j("#cancelEditBossConf").click(function(e) {
		//alert('');
		$j("#saveBossConf").css("display", "none");
		$j("#cancelEditBossConf").css("display", "none");
		$j("#editBossConf").css("display", "block");
		$j("#ipInputBox").css("display", "none");
		$j("#ipInputBoxDisabled").css("display", "block");
		toggleEnableDisable(true);
		if(Object.keys(objConfigurationScreen).length > 0){
			bindScreenData(objConfigurationScreen);
		}

	});

	$j("#btn_remove_logs").click(function(e) {
		$j("#btn_delete_logs").css("display", "block");
		$j("#btn_cancel_delete").css("display", "block");
		$j("#btn_remove_logs").css("display", "none");
		toggleEnableDisableDeleteLogsTab(false);
	});

	$j("#btn_cancel_delete").click(function(e) {
		$j("#btn_delete_logs").css("display", "none");
		$j("#btn_cancel_delete").css("display", "none");
		$j("#btn_remove_logs").css("display", "block");
		$j('#select_log_type').prop('selectedIndex',0);
		toggleEnableDisableDeleteLogsTab(true);
		document.getElementById("to_date").value = "";
		document.getElementById("from_date").value = "";
	});


	$j('#chkAllLogs').click(function(event) {
		if(this.checked) {
			$j('[name="enableLog"]').each(function() {
				this.checked = true;
			});
		}
		else {
			$j('[name="enableLog"]').each(function() {
				this.checked = false;
			});
		}
	});

	$j('[name="enableLog"]').on('click', function(){
		manageCheckAll();
	});

	function manageCheckAll(){
		var flag_check = true;
		$j('[name="enableLog"]').each(function() {
			if(!this.checked){
				flag_check = false;
			}
		});
		$j('#chkAllLogs').prop('checked', flag_check);
	}

	$j("#saveBossConf").click(function(e) {
			//alert(($j('#chkEventLog').is(":checked")));
		$j.ajax({
			url : "checkSession",
			type : "POST",
			dataType : "json",
			success : saveBossConf
		});
	});

	function saveBossConf(result) {
		if(result.status == 208){
			if(isValid()){
				var deviceIP1List = document.getElementById("deviceIP1").value;
				var deviceIP2List = document.getElementById("deviceIP2").value;
				var deviceIP3List = document.getElementById("deviceIP3").value;
				var deviceIP4List = document.getElementById("deviceIP4").value;
				var data = {
				BOSS_IP_ADDRESS : deviceIP1List+'.'+ deviceIP2List +'.'+ deviceIP3List +'.'+ deviceIP4List,
				SNMP_RECEIVER_IP_ADDRESS : $j("#txt_snmp_ip").val(),
				VOLTAGE_RED_THRESHOLD : $j("#text_red_threshold").val(),
				VOLTAGE_YELLOW_THRESHOLD : $j("#text_yellow_threshold").val(),
				EVENT_LOG_FLAG : $j("#chkEventLog").is(":checked") == 1 ? "true" : "false",
				SWOP_LOG_FLAG : $j("#chkSwopLog").is(":checked") == 1 ? "true" : "false",
				MAINTENANCE_LOG_FLAG : $j("#chkMaintLog").is(":checked") == 1 ? "true" : "false",
				MOTOR_CURRENT_LOG_FLAG : $j("#chkCurrentLog").is(":checked") == 1 ? "true" : "false",
				LOG_POL_RATE : '*/' + $j("#allLogsDataPoll").val() + ' *'+ ($j("#allLogsDataPoll_hours").val() == 0 ? "" : "/"+ $j("#allLogsDataPoll_hours").val()) + ' * * *',
				INS_DATA_POL_RATE : '*/' + $j("#instDataPoll").val() + ' *'+ ($j("#instDataPoll_hours").val() == 0 ? "" : "/" + $j("#instDataPoll_hours").val()) + ' * * *',
				SNMP_LOG_FLAG : $j("#chkEnableSnmpLog").is(":checked") == 1 ? "true" : "false",
				SNMP_LOG_MAX_SIZE : $j("#snmpLogMaxSize").val(),
				MOTOR_CURR_LOG_MAX_SIZE : $j("#currentLogMaxSize").val()
			};
			console.log(data);
			socket.emit('updBossConf',data);
			//console.log(data);
			socket.on('updBossConfUpdated',function(message){
				ShowAlert("ASTS Preferences saved successfully.", function(){
					location.reload();
				});
			});
		}

		}else{
			document.getElementById('configuration_menu').dispatchEvent(event);
		}
	}

	function isValid(){
		if($j("#text_red_threshold").val().trim() == ""){
			ShowAlert ("RED Threshold For Low IECC Voltage can't be blank");
			return false;
		}
		if($j("#text_yellow_threshold").val().trim() == ""){
			ShowAlert ("YELLOW Threshold For Low IECC Voltage can't be blank");
			return false;
		}
		if(!validateForNumeric($j("#text_red_threshold").val())){
			ShowAlert ("RED Threshold For Low IECC Voltage must be numeric");
			return false;
		}
		if(!validateForNumeric($j("#text_yellow_threshold").val())){
			ShowAlert ("YELLOW Threshold For Low IECC Voltage must be numeric");
			return false;
		}
		var deviceIP1List = document.getElementById("deviceIP1").value;
		var deviceIP2List = document.getElementById("deviceIP2").value;
		var deviceIP3List = document.getElementById("deviceIP3").value;
		var deviceIP4List = document.getElementById("deviceIP4").value;
		if(isEmpty(deviceIP1List, deviceIP2List, deviceIP3List, deviceIP4List)){
			ShowAlert ("ASTS IP Address fields can't be blank.");
			return false;
		}
		return true;
	}

	function isEmpty(a,b,c,d){
	  if (a!="" && b!="" && c!="" && d!=""){
	    return false;
	  }
	  else return true;
	}

	function getConfigurationData(){
		socket.emit('get-Conf-data',function(data){
			objConfigurationScreen.BOSS_IP_ADDRESS = data.find(function(x){ return x.KEY === 'BOSS_IP_ADDRESS' }).VALUE;
			objConfigurationScreen.VOLTAGE_RED_THRESHOLD = data.find(function(x){ return x.KEY === 'VOLTAGE_RED_THRESHOLD'}).VALUE;
			objConfigurationScreen.VOLTAGE_YELLOW_THRESHOLD = data.find(function(x){ return x.KEY === 'VOLTAGE_YELLOW_THRESHOLD'}).VALUE;
			objConfigurationScreen.EVENT_LOG_FLAG = data.find(function(x){ return x.KEY === 'EVENT_LOG_FLAG'}).VALUE;
			objConfigurationScreen.SWOP_LOG_FLAG = data.find(function(x){ return x.KEY === 'SWOP_LOG_FLAG'}).VALUE;
			objConfigurationScreen.MAINTENANCE_LOG_FLAG = data.find(function(x){ return x.KEY === 'MAINTENANCE_LOG_FLAG'}).VALUE;
			objConfigurationScreen.MOTOR_CURRENT_LOG_FLAG = data.find(function(x){ return x.KEY === 'MOTOR_CURRENT_LOG_FLAG'}).VALUE;
			objConfigurationScreen.SNMP_LOG_FLAG = data.find(function(x){ return x.KEY === 'SNMP_LOG_FLAG'}).VALUE;
			objConfigurationScreen.SNMP_LOG_MAX_SIZE = data.find(function(x){ return x.KEY === 'SNMP_LOG_MAX_SIZE'}).VALUE;
			objConfigurationScreen.MOTOR_CURR_LOG_MAX_SIZE = data.find(function(x){ return x.KEY === 'MOTOR_CURR_LOG_MAX_SIZE'}).VALUE;
			objConfigurationScreen.LOG_POL_RATE = data.find(function(x){ return x.KEY === 'LOG_POL_RATE'}).VALUE;
			objConfigurationScreen.INS_DATA_POL_RATE = data.find(function(x){ return x.KEY === 'INS_DATA_POL_RATE'}).VALUE;
			//console.log(objConfigurationScreen);
			bindScreenData(objConfigurationScreen);
		});
	}

	function bindScreenData(obj){
		console.log(obj);
		document.getElementById("deviceIP1").value = obj.BOSS_IP_ADDRESS.split('.')[0];
		document.getElementById("deviceIP2").value = obj.BOSS_IP_ADDRESS.split('.')[1];
		document.getElementById("deviceIP3").value = obj.BOSS_IP_ADDRESS.split('.')[2];
		document.getElementById("deviceIP4").value = obj.BOSS_IP_ADDRESS.split('.')[3];
		$j("#txt_boss_ip").val(obj.BOSS_IP_ADDRESS);
		$j("#text_red_threshold").val(obj.VOLTAGE_RED_THRESHOLD);
		$j("#text_yellow_threshold").val(obj.VOLTAGE_YELLOW_THRESHOLD)
		$j("#chkEventLog").prop('checked', obj.EVENT_LOG_FLAG == "true" ? 1 : 0);
		$j("#chkSwopLog").prop('checked', obj.SWOP_LOG_FLAG == "true" ? 1 : 0);
		$j("#chkMaintLog").prop('checked', obj.MAINTENANCE_LOG_FLAG == "true" ? 1 : 0);
		$j("#chkCurrentLog").prop('checked', obj.MOTOR_CURRENT_LOG_FLAG == "true" ? 1 : 0) ;
		$j("#chkEnableSnmpLog").prop('checked', obj.SNMP_LOG_FLAG == "true" ? 1 : 0);
		$j("#snmpLogMaxSize").val(obj.SNMP_LOG_MAX_SIZE);
		$j("#snmp-max-storage-value").html(obj.SNMP_LOG_MAX_SIZE + " GB");
		SNMPLogMaxSize = obj.SNMP_LOG_MAX_SIZE;
		$j("#currentLogMaxSize").val(obj.MOTOR_CURR_LOG_MAX_SIZE);
		AllLogsMaxZize = obj.MOTOR_CURR_LOG_MAX_SIZE;
		$j("#events-max-storage-value").html(obj.MOTOR_CURR_LOG_MAX_SIZE + " GB");
		$j("#motor-current-max-storage-value").html(obj.MOTOR_CURR_LOG_MAX_SIZE + " GB");
		$j("#swop-max-storage-value").html(obj.MOTOR_CURR_LOG_MAX_SIZE + " GB");
		$j("#maintenance-max-storage-value").html(obj.MOTOR_CURR_LOG_MAX_SIZE + " GB");
		$j("#allLogsDataPoll").val(obj.LOG_POL_RATE.split('/').length -1 == 1 ? obj.LOG_POL_RATE.split('/')[1].substr(0,1) : 1);
		$j("#allLogsDataPoll_hours").val(obj.LOG_POL_RATE.split('/').length -1 == 2 ? obj.LOG_POL_RATE.split('/')[2].substr(0,1) : 0);
		$j("#instDataPoll").val(obj.INS_DATA_POL_RATE.split('/').length -1  == 1 ? obj.INS_DATA_POL_RATE.split('/')[1].substr(0,1) : 1);
		$j("#instDataPoll_hours").val(obj.INS_DATA_POL_RATE.split('/').length -1 == 2 ? obj.INS_DATA_POL_RATE.split('/')[2].substr(0,1) : 0);
		socket.emit('get-disk-space');
		socket.emit('getLogsSize');
		manageCheckAll();
	}

	$j("#from_date").click(function(e) {
		 //  alert('h');
		 e.stopPropagation();
		})

	$j('#from_date').datepicker({
		//alert('dtPickr');
		language : 'en',
		autoClose : 'true',
		dateFormat : "yyyy-mm-dd",
		position : 'top left',
		minDate: new Date(), // Now can select only dates, which goes after today
		maxDate: new Date(),
		onSelect : function onSelect(fd, date) {
		}
	})

	$j("#to_date").click(function(e) {
		 //  alert('h');
		 e.stopPropagation();
		})

	$j('#to_date').datepicker({
		//alert('dtPickr');
		language : 'en',
		autoClose : 'true',
		dateFormat : "yyyy-mm-dd",
		position : 'top left',
		minDate: new Date(), // Now can select only dates, which goes after today
		maxDate: new Date(),
		onSelect : function onSelect(fd, date) {
		}
	})


	function toggleEnableDisable(enableFlag){
		$j('[name="enableLog"]').each(function() {
			this.disabled= enableFlag;
		});
		$j('#chkAllLogs').prop('disabled', enableFlag);
		$j('[type="number"]').each(function() {
			this.disabled = enableFlag;
		})
		$j('#text_red_threshold').prop('disabled', enableFlag);
		$j('#text_yellow_threshold').prop('disabled', enableFlag);
		$j('#txt_boss_ip').prop('disabled', true);
		$j('#txt_snmp_ip').prop('disabled', true);

	}

	function toggleEnableDisableDeleteLogsTab(enableFlagForDeleteLogs){
		$j('#from_date').prop('disabled', enableFlagForDeleteLogs);
		$j('#to_date').prop('disabled', enableFlagForDeleteLogs);
		$j('#select_log_type').prop('disabled', enableFlagForDeleteLogs);
	}

	function validateForNumeric(fieldValue) {
		var intPage = parseInt(fieldValue);
		if (intPage.toString() != fieldValue) {
			return false;
		}
		return true;
	} 

	function handleIPAddressInputs(event){
    if(event.keyCode == 8 || event.keyCode == 9 || event.keyCode ==  46 || event.keyCode == 37 || event.keyCode == 39){
      if(event.keyCode == 8 && event.target.value.length == 0){
          switch(event.target.id) {
            case 'deviceIP1':
                return true;
                break;
            case 'deviceIP2':
                event.target.parentElement.querySelector("#deviceIP1").focus();
                return true;
                break;
            case 'deviceIP3':
                event.target.parentElement.querySelector("#deviceIP2").focus();
                return true;
                break;
            case 'deviceIP4':
                event.target.parentElement.querySelector("#deviceIP3").focus();
                return true;
                break;
            default:
                return true;
        }
      }
      return true;
    }
    if (!/^\d+$/.test(String.fromCharCode(event.keyCode))) // Check if input is a number;
      return false;
    else if(event.target.value.length == 3){
        switch(event.target.id) {
          case 'deviceIP1':
              event.target.parentElement.querySelector("#deviceIP2").focus();
              return true;
              break;
          case 'deviceIP2':
              event.target.parentElement.querySelector("#deviceIP3").focus();
              return true;
              break;
          case 'deviceIP3':
              event.target.parentElement.querySelector("#deviceIP4").focus();
              return true;
              break;
          default:
              return true;
      }
    }
  }

  document.getElementById("deviceIP1").onkeydown = handleIPAddressInputs;
  document.getElementById("deviceIP2").onkeydown = handleIPAddressInputs;
  document.getElementById("deviceIP3").onkeydown = handleIPAddressInputs;
  document.getElementById("deviceIP4").onkeydown = handleIPAddressInputs;

	$j('#allLogsDataPoll').change(function() {
		var errorMessage = 'Long term data polling rate minute value must be interger and in range from 1 to 60';
		if ((! validateForNumeric(this.value)) || this.value > 60 || this.value < 1) {
			$j("#allLogsDataPoll").val(objConfigurationScreen.LOG_POL_RATE.split('/')[1].substr(0,1));
			ShowAlert (errorMessage);
		}
	});

	$j('#allLogsDataPoll_hours').change(function() {
		var errorMessage = 'Long term data polling rate hour value must be interger and in range from 0 to 23';
		if ((!validateForNumeric(this.value)) || this.value > 23 || this.value < 0) {
			$j("#allLogsDataPoll_hours").val(objConfigurationScreen.LOG_POL_RATE.split('/')[2].substr(0,1));
			ShowAlert (errorMessage);
		}
	});

	$j('#instDataPoll').change(function() {
		var errorMessage = 'Instantaneous data polling rate minute value must be interger and in range from 1 to 60';
		if ((!validateForNumeric(this.value)) || this.value > 60 || this.value < 1) {
			$j("#instDataPoll").val(objConfigurationScreen.INS_DATA_POL_RATE.split('/')[1].substr(0,1));
			ShowAlert (errorMessage);
		}
	});

	$j('#instDataPoll_hours').change(function() {
		var errorMessage = 'Instantaneous data polling rate hour value must be interger and in range from 0 to 23';
		if ((!validateForNumeric(this.value)) || this.value > 23 || this.value < 0) {
			$j("#instDataPoll_hours").val(objConfigurationScreen.INS_DATA_POL_RATE.split('/')[2].substr(0,1));
			ShowAlert (errorMessage);
		}
	});

	$j('#currentLogMaxSize').change(function() {
		var errorMessage ='Motor current log max storage size must be numeric and in renge between 1 to 1000';
		if ((!validateForNumeric(this.value)) || this.value < 1 || this.value > 1000) {
			$j("#currentLogMaxSize").val(objConfigurationScreen.MOTOR_CURR_LOG_MAX_SIZE);
			ShowAlert (errorMessage);
		}
	});

	$j('#snmpLogMaxSize').change(function() {
		var errorMessage = 'SNMP log max storage size must be numeric and in renge between 1 to 1000';
		if ((!validateForNumeric(this.value)) || this.value < 1 || this.value > 1000) {
			$j("#snmpLogMaxSize").val(objConfigurationScreen.SNMP_LOG_MAX_SIZE);
			ShowAlert (errorMessage);
		}
	});


	socket.on('logStorageInfo',function(objLogStorageInfo){
		if(objLogStorageInfo.log_name == 'snmp')
			showStorageGraph(objLogStorageInfo.log_name, SNMPLogMaxSize, objLogStorageInfo.logsize, objLogStorageInfo.thershold);
		else
			showStorageGraph(objLogStorageInfo.log_name, AllLogsMaxZize, objLogStorageInfo.logsize, objLogStorageInfo.thershold);
	});

	socket.on('disk-space', function(objDiskSpace){
		var MBPerPixel = ((1024 * objDiskSpace.totaldiskSpace) / 150).toFixed(1);
		var GraphLengthInPixels = ((objDiskSpace.usedSpace * 1024) / MBPerPixel).toFixed(1);
		var ThresholdPixels = (((1024 * objDiskSpace.totaldiskSpace * objDiskSpace.threshold)/ 100 )/ MBPerPixel).toFixed(1);
		var graphValueInGB = objDiskSpace.usedSpace;
		$j("#disk-storage-graph").css("width", GraphLengthInPixels);
		$j("#disk-storage-value").html(graphValueInGB + " GB");
		$j("#disk-max-storage-value").html(objDiskSpace.totaldiskSpace + " GB");
		if(parseFloat(GraphLengthInPixels) > parseFloat(ThresholdPixels)){
			$j("#disk-storage-graph").css("background-color", "#ff4747");
		}
	})

  	//showStorageGraph('events', 5, 1024, 95);
  	//showStorageGraph('switch_operations', 5, 4920, 95);

  	function showStorageGraph(LogName, MaxSize, CurrentSize, Threshold){
  		var MBPerPixel = ((1024 * MaxSize) / 150).toFixed(1);
  		var GraphLengthInPixels = (CurrentSize / MBPerPixel).toFixed(1);
  		var ThresholdPixels = (((1024 * MaxSize * Threshold)/ 100 )/ MBPerPixel).toFixed(1);
  		var graphValueInGB = (CurrentSize / 1024).toFixed(1);
  		switch(LogName) {
  			case "events":
  			$j("#events-storage-graph").css("width", GraphLengthInPixels);
  			$j("#events-storage-value").html(graphValueInGB + " GB");
  			if(parseFloat(GraphLengthInPixels) > parseFloat(ThresholdPixels)){
  				$j("#events-storage-graph").css("background-color", "#ff4747");
  			}
  			break;
  			case "switch_operations":
  			$j("#swop-storage-graph").css("width", GraphLengthInPixels);
  			$j("#swop-storage-value").html(graphValueInGB + " GB");
  			if(parseFloat(GraphLengthInPixels) > parseFloat(ThresholdPixels)){
  				$j("#swop-storage-graph").css("background-color", "#ff4747");
  			}
  			break;
  			case "maintenance":
  			$j("#maintenance-storage-graph").css("width", GraphLengthInPixels);
  			$j("#maintenance-storage-value").html(graphValueInGB + " GB");
  			if(parseFloat(GraphLengthInPixels) > parseFloat(ThresholdPixels)){
  				$j("#maintenance-storage-graph").css("background-color", "#ff4747");
  			}
  			break;
  			case "snmp":
  			$j("#snmp-storage-graph").css("width", GraphLengthInPixels);
  			$j("#snmp-storage-value").html(graphValueInGB + " GB");
  			if(parseFloat(GraphLengthInPixels) > parseFloat(ThresholdPixels)){
  				$j("#snmp-storage-graph").css("background-color", "#ff4747");
  			}
  			break;
  			case "motor_current":
  			$j("#motor-current-storage-graph").css("width", GraphLengthInPixels);
  			$j("#motor-current-storage-value").html(graphValueInGB + " GB");
  			if(parseFloat(GraphLengthInPixels) > parseFloat(ThresholdPixels)){
  				$j("#motor-current-storage-graph").css("background-color", "#ff4747");
  			}
  			break;
  			default:

  		}
  	}

  	$j("#select_log_type").change(function() {
  		socket.emit('get-mix-and-max-date', this.value);
  	});

  	socket.on('min-max-date', function(data){
  		if(data.minDate == null && data.maxDate == null){
  			$j('#from_date').prop('disabled', true);
			$j('#to_date').prop('disabled', true);
			ShowAlert("No data found for delete.");
  		}
  		else{
  			$j('#from_date').prop('disabled', false);
			$j('#to_date').prop('disabled', false);
	  		$j('#from_date').datepicker({
				//alert('dtPickr');
				language : 'en',
				autoClose : 'true',
				dateFormat : "yyyy-mm-dd",
				position : 'top left',
				minDate: new Date(data.minDate),
				maxDate: new Date(data.maxDate),
				onSelect : function onSelect(fd, date) {
					$j('#to_date').datepicker({
						minDate: date
					});
					//document.getElementById("to_date").value = new Date(date.getTime() - (date.getTimezoneOffset() * 60000)).toISOString().slice(0,10);
				}
			});

	  		$j('#to_date').datepicker({
				//alert('dtPickr');
				language : 'en',
				autoClose : 'true',
				dateFormat : "yyyy-mm-dd",
				position : 'top left',
				minDate: new Date(data.minDate),
				maxDate: new Date(data.maxDate),
				onSelect : function onSelect(fd, date) {
				}
			})
  		}

  	});

  	function isValidInputs(logName, minDate, maxDate){
  		if(logName == ""){
  			ShowAlert("Please select log type to be deleted.");
  			return false;
  		}
  		if(minDate.trim() == "" || maxDate.trim() == ""){
  			ShowAlert("To date and from date can't be blank.");
  			return false;
  		}
  		if(new Date(minDate) > new Date(maxDate)){
  			ShowAlert("From date can't be grater than To date.");
  			return false;
  		}
  		return true;
  	}

  	$j("#btn_delete_logs").click(function(e) {
  		if(isValidInputs($j('#select_log_type').val(), $j('#from_date')[0].value, $j('#to_date')[0].value)){
  			var message = "Are you sure you want to delete ? " + $j('#select_log_type :selected').text() + " data for all IECC devices will be delete between " + $j('#from_date')[0].value + " and " + $j('#to_date')[0].value + " dates.";
	  		ShowConfirm(message, function(){
	  			var params = {
	  				logName : $j('#select_log_type').val(),
	  				minDate : $j('#from_date')[0].value,
	  				maxDate : $j('#to_date')[0].value,
	  			}
	  			socket.emit('delete-logs', params, function(objStatus){
	  				if(objStatus.status == 'ok'){
	  					ShowAlert("data successfully deleted.", function(){
	  						location.reload();
	  					});
	  				}
	  			});
	  		});
  		}
  	})
  });