jQuery( document ).ready(function( $ ) {
  var socket = io();

  // Function to add new input fields for device entry.
  function addAnotherDeviceEntryForm(){
    var element = document.createElement("a");
    element.text = "✖";
    element.classList.add("remove-button");
    element.onclick = function(){
      $(this).parent().remove();
    }
    var itm = document.getElementById("device-entry-form");
    var cln = itm.cloneNode(true);
    cln.classList.add("form-separator");
    cln.insertBefore(element, cln.firstChild);
    cln.querySelector("#device_name").value = "";
    cln.querySelector("#deviceIP1").value = "";
    cln.querySelector("#deviceIP1").onkeydown = handleIPAddressInputs;
    cln.querySelector("#deviceIP2").value = "";
    cln.querySelector("#deviceIP2").onkeydown = handleIPAddressInputs;
    cln.querySelector("#deviceIP3").value = "";
    cln.querySelector("#deviceIP3").onkeydown = handleIPAddressInputs;
    cln.querySelector("#deviceIP4").value = "";
    cln.querySelector("#deviceIP4").onkeydown = handleIPAddressInputs;
    cln.querySelector("#device_port").value = "";
    cln.querySelector("#btn_check_ip").onclick = checkIfExist;
    document.getElementById("device-entry-area").appendChild(cln);
  }

  // Function to add IECC device.
  function addDevice(){
    var deviceEntryList = [];
    var deviceCount = document.getElementsByName("device-entry-form").length;
    var categoryList = document.getElementsByName("device_category");
    var deviceNameList = document.getElementsByName("device_name");
    var deviceIP1List = document.getElementsByName("deviceIP1");
    var deviceIP2List = document.getElementsByName("deviceIP2");
    var deviceIP3List = document.getElementsByName("deviceIP3");
    var deviceIP4List = document.getElementsByName("deviceIP4");
    var portList = document.getElementsByName("device_port");
    var isValid = true;
    checkForDuplicateNameAndIP(deviceNameList, deviceIP1List, deviceIP2List, deviceIP3List, deviceIP4List, portList, function(response){
      isValid = response;
      for (var i = 0; i < deviceCount; i++){
        if(!isEmpty(deviceNameList[i].value.trim(), portList[i].value, deviceIP1List[i].value, deviceIP2List[i].value, deviceIP3List[i].value, deviceIP4List[i].value)){
          var objDevice = {
            name : deviceNameList[i].value.trim(),
            category : categoryList[i].options[categoryList[i].selectedIndex].value,
            port : portList[i].value,
            ip : deviceIP1List[i].value +'.'+ deviceIP2List[i].value +'.'+ deviceIP3List[i].value +'.'+ deviceIP4List[i].value
          }
          deviceEntryList.push(objDevice);
        }
        else {
          isValid = false;
          ShowAlert("Blank fields are not allowed. Please fill all the fields.");
          break;
        }
      }
      if(isValid){
        socket.emit('add-new-devices', deviceEntryList, function(data){
          if(data.status == 'ok'){
            ShowAlert("Device added successfully.", function(){
              location.reload();
            });
          }
          else{
            ShowAlert(data.message);
          }
        });
      }
    });
  }

  function checkForDuplicateNameAndIP(deviceNameList, deviceIP1List, deviceIP2List, deviceIP3List, deviceIP4List, portList, callback){
    debugger;
    ValidateNameList(deviceNameList, function(isValid){
      if(isValid){
        ValidateIPList(deviceIP1List, deviceIP2List, deviceIP3List, deviceIP4List, portList, function(isValidIPs){
          if(isValidIPs){
            callback(true);
          }
          else{
            callback(false);
          }
        });
      }
      else{
        callback(false);
      }
    });
  }

  function ValidateNameList(deviceNameList, callback){
    var localIpDeviceNameList = [];
    for(var i = 0; i < deviceNameList.length; i++){
      localIpDeviceNameList.push(deviceNameList[i].value);
    }
    var DeviceNameUniqueCollection = new Set();
    var lowerCaseDeviceNameList = localIpDeviceNameList.map(function(x){return x.toLowerCase()});
    var counter = 0;
    for (var i = 0; i< lowerCaseDeviceNameList.length; i++){
      DeviceNameUniqueCollection.add(lowerCaseDeviceNameList[i]);
      counter ++;
      if(counter == lowerCaseDeviceNameList.length){
        if (localIpDeviceNameList.length !== DeviceNameUniqueCollection.size){
          ShowAlert("Device name must be unique.");
          callback(false);
        }
        else{
          callback(true);
        }
      }
    }
  }

  function ValidateIPList (deviceIP1List, deviceIP2List, deviceIP3List, deviceIP4List, portList, callback){
    var localIpList = [];
    for(var i = 0; i < deviceIP1List.length; i++){
      localIpList.push(deviceIP1List[i].value +'.'+ deviceIP2List[i].value +'.'+ deviceIP3List[i].value +'.'+ deviceIP4List[i].value +':'+ portList[i].value)
    }
    var DeviceIPUniqueCollection = new Set();
    var lowerCaseDeviceIPList = localIpList.map(function(x){return x.toLowerCase()});
    var counter2 = 0;
    for (var j = 0; j< lowerCaseDeviceIPList.length; j++){
      DeviceIPUniqueCollection.add(lowerCaseDeviceIPList[j]);
      counter2 ++;
      if(counter2 == lowerCaseDeviceIPList.length){
        if(localIpList.length !== DeviceIPUniqueCollection.size){
          ShowAlert("Device IP must be unique.");
          callback(false);
        }
        else{
          callback(true);
        }
      }
    }
  }

  function isEmpty(a,b,c,d,e,f){
    if (a!="" && b!="" && c!="" && d!="" && e!="" && f!=""){
      return false;
    }
    else return true;
  }

  function handleIPAddressInputs(event){
    if(event.keyCode == 8 || event.keyCode == 9 || event.keyCode ==  46 || event.keyCode == 37 || event.keyCode == 39){
      if(event.keyCode == 8 && event.target.value.length == 0){
        switch(event.target.id) {
          case 'deviceIP1':
          return true;
          break;
          case 'deviceIP2':
          event.target.parentElement.querySelector("#deviceIP1").focus();
          return true;
          break;
          case 'deviceIP3':
          event.target.parentElement.querySelector("#deviceIP2").focus();
          return true;
          break;
          case 'deviceIP4':
          event.target.parentElement.querySelector("#deviceIP3").focus();
          return true;
          break;
          default:
          return true;
        }
      }
      return true;
    }
    if (!/^\d+$/.test(String.fromCharCode(event.keyCode))) // Check if input is a number;
    return false;
    else if(event.target.value.length == 3){
      switch(event.target.id) {
        case 'deviceIP1':
        event.target.parentElement.querySelector("#deviceIP2").focus();
        return true;
        break;
        case 'deviceIP2':
        event.target.parentElement.querySelector("#deviceIP3").focus();
        return true;
        break;
        case 'deviceIP3':
        event.target.parentElement.querySelector("#deviceIP4").focus();
        return true;
        break;
        default:
        return true;
      }
    }
  }

  // function to check if device exist.
  function checkIfExist(event){
    var entryForm = event.target.parentNode.parentNode;
    entryForm.querySelector("#busy_gif").style.display="block";
    var url = 'http://' + entryForm.querySelector("#deviceIP1").value.trim()
    +'.'+ entryForm.querySelector("#deviceIP2").value.trim()
    +'.'+ entryForm.querySelector("#deviceIP3").value.trim()
    +'.'+ entryForm.querySelector("#deviceIP4").value.trim()
    +':'+ entryForm.querySelector("#device_port").value.trim()
    +'/api/getStaticInfo'
    $.ajax({
      type: "GET",
      url: url,
      data:"",
      dataType:"text",
      contentType: "text/html",
      success: function(){
        entryForm.querySelector("#busy_gif").style.display="none";
        ShowAlert("Device found");
          //entryForm.querySelector("#check_status_result").classList.add("deviceAvailable");
        },
        error: function(requestObject, error, errorThrown) {
          entryForm.querySelector("#busy_gif").style.display="none";
          ShowAlert("Device not found");
          //entryForm.querySelector("#check_status_result").classList.add("deviceNotAvailable");
        },
        timeout: 3000
      });
  }
  // bind methods with their events
  document.getElementById("deviceIP1").onkeydown = handleIPAddressInputs;
  document.getElementById("deviceIP2").onkeydown = handleIPAddressInputs;
  document.getElementById("deviceIP3").onkeydown = handleIPAddressInputs;
  document.getElementById("deviceIP4").onkeydown = handleIPAddressInputs;
  document.getElementById("btn_check_ip").onclick = checkIfExist;
  document.getElementById("btn_add_another_device").onclick = addAnotherDeviceEntryForm;
  document.getElementById("btn_add_all_device").onclick = addDevice;
});
