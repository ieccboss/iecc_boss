(function ( $, window, undefined ) {
  var expandedNodeList = [];
  var settings;

  // workaround for IE
  if(!Array.prototype.includes){
    Array.prototype.includes = function(){
      'use strict';
      return Array.prototype.indexOf.apply(this, arguments) !== -1;
    };
  }

  var methods = {
    init : function(options) {

      // Default Settings
      settings = $.extend({
        expandIconClass: 'closed',
        contractIconClass: 'open',
        toggleButtonClass: 'toggle',
        animateActions: false,
        openAnimation: {
          height: "show",
          marginTop: "show",
          marginBottom: "show",
          paddingTop: "show",
          paddingBottom: "show"
        },
        openAnimationSpeed: 500,
        closeAnimation: {
          height: "hide",
          marginTop: "hide",
          marginBottom: "hide",
          paddingTop: "hide",
          paddingBottom: "hide"
        },
        closeAnimationSpeed: 100,
        reveal: undefined
      }, options);

      return this.each(function() {
        var target = $(this);

        target.find('li').each(function() {
          var node = $(this),
             branches = node.children('ul, ol');

          if(!node.data("loaded") && branches.length > 0)
          {

            branches.hide();
            node.prepend(methods.openCloseButton(branches));
            node.data("loaded", true);

          }

        });

        if(undefined !== settings.reveal)
        {
          methods.reveal(settings.reveal);
        }

      });
    },

    reveal : function(element) {

      $(element).parents('li').each(function() {
        $(this).children('div.' + settings.toggleButtonClass).click();
      });

    },

    openCloseButton : function(branches) {
      var button = $('<div />',
      {
        'class': settings.toggleButtonClass + ' ' + settings.expandIconClass,
        on: {
          click: function(event) {
            var self = button;
            var parentID = self.closest('li').attr('id');
            if (expandedNodeList.includes(parentID)){
              expandedNodeList.splice(expandedNodeList.indexOf(parentID), 1);
              self.data('open', true);
            }
            else {
              expandedNodeList.push(parentID);
              self.data('open', false);
            }
            methods.animateActions(branches, self.data('open'));
            (self.data('open'))
              ? button.removeClass(settings.contractIconClass).addClass(settings.expandIconClass)
              : button.removeClass(settings.expandIconClass).addClass(settings.contractIconClass);

              //self.data('open', !self.data('open'));

          }
        }
      });

      // Initialize buttons as closed
      button.data('open', false);

      return button;
    },

    animateActions : function(branches, open) {
      if(settings.animateActions)
      {
        (open)
          ? branches.animate(settings.closeAnimation, settings.closeAnimationSpeed)
          : branches.animate(settings.openAnimation, settings.openAnimationSpeed);
      }
      else
      {
        branches.toggle();
      }
    }
  }

  $.fn.goodtree = function( method, callback ) {
    if ( methods[method] ) {
        methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        callback(expandedNodeList);
        return;
      } else if ( typeof method === 'object' || ! method ) {
        methods.init.apply( this, arguments );
        callback(expandedNodeList);
        return;
      } else {
        $.error( 'Method ' +  method + ' does not exist on jQuery.goodtree' );
      }
  };

}) ( jQuery, window );
