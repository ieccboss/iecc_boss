var mysql = require('mysql');
var config_connection_obj = require('./db_connection.json');
var connection = mysql.createConnection(config_connection_obj);
var multiQueryConnectionObj = {
  "host": config_connection_obj.host,
  "user": config_connection_obj.user,
  "password": config_connection_obj.password,
  "database": config_connection_obj.database,
  "port" : config_connection_obj.port,
  "multipleStatements" : true
}
var multipleQueryConnection = mysql.createConnection(multiQueryConnectionObj);
var fs = require('fs');
var path = require('path');
var dir = path.join(__dirname,'repo/');
connection.connect();
multipleQueryConnection.connect();

module.exports = function(io){

  function _GetTreeData(callback){
    if(typeof global.objDeviceList !== 'undefined' && typeof global.objCategoryList !== 'undefined'){
      var data = {categoryList : global.objCategoryList, deviceList : global.objDeviceList};
      callback(undefined, data);
    }
    else{
      connection.query('select id as cat_id, cat_name from tbl_devices_category order by cat_name;', function(err, rows) {
        var categoryList = rows;
        global.objCategoryList = rows;
        if(err){
          callback(err, {});
        }

        var deviceDataQuery = "select id as dev_id, name as dev_name, category as cat_id, device_ip as ip,"
        + "is_connected,"
        + "ifnull(latch_out_status,'NLO') latch_out_status,"
        + "ifnull(health_status, 'G') health_status,"
        + "ifnull(aux_sensors_value, 0) aux_sensors_value,"
        + "ifnull(aux_sensors_threshold, 0) aux_sensors_threshold,"
        + "ifnull(motor_current_value, 0) motor_current_value,"
        + "ifnull(motor_current_threshold, 0) motor_current_threshold,"
        + "ifnull(motor_throw_time_value, 0) motor_throw_time_value,"
        + "ifnull(motor_throw_time_threshold, 0) motor_throw_time_threshold,"
        + "ifnull(voltage_value, 0) voltage_value"
        //+ "ifnull(voltage_red_threshold, 0) voltage_red_threshold,"
        //+ "ifnull(voltage_yellow_threshold, 0) voltage_yellow_threshold"
        + " from tbl_devices group by cat_id, dev_id, dev_name order by dev_name;";

        connection.query(deviceDataQuery, function(err, rows) {
          var deviceList = rows;
          global.objDeviceList = rows;
          connection.query("select `KEY`, CAST(ifnull(`VALUE`,0) as SIGNED) as `VALUE` FROM config where `KEY` in ('VOLTAGE_YELLOW_THRESHOLD', 'VOLTAGE_RED_THRESHOLD');", function(error,data){
            if(data.length > 1){
              for(var i = 0; i < deviceList.length; i++){
                deviceList[i][data[0]['KEY'].toLowerCase()] = data[0]['VALUE'];
                deviceList[i][data[1]['KEY'].toLowerCase()] = data[1]['VALUE'];
              }
              global.objDeviceList = deviceList;
            }
            var data = {categoryList : categoryList, deviceList : deviceList};
            callback(err, data);
          });
        });
      });
    }
  }

  function _UpdateDeviceCategory(dev_id, cat_id, callback){
    //console.log('update tbl_devices set category = "'+ cat_id +'" where id = "'+ dev_id +'";');
    connection.query('update tbl_devices set category = ? where id = ?;', [cat_id, dev_id], function(err){
      if(err){
        callback(err, {});
      }
      delete global.objCategoryList;
      delete global.objDeviceList;
      _GetTreeData(callback);
    });
  }

  function _AddNewDevices(deviceList, callback){
    validateDeviceNames(deviceList, function(objStatus){
      if(objStatus.isvalid){
        validateDeviceIP(deviceList, function(objStatusIp){
          if(objStatusIp.isvalid){
            //connection.beginTransaction();
            connection.beginTransaction(function(err) {
              if (err) { throw err; }
              var counter = 0;
              for (var i = 0; i < deviceList.length; i++) {
                connection.query('insert into tbl_devices (name, category, port, device_ip, created_on, is_connected, initial_poll, initial_poll_processing) values (?,?,?,?,CURRENT_TIMESTAMP,"N","1111","0000")', [deviceList[i].name, deviceList[i].category, deviceList[i].port, deviceList[i].ip], function(err){
                  if(err)
                    callback({status : "error", message : err});
                  else{
                    connection.query('select id from tbl_devices where device_ip = ?;', deviceList[counter++].ip, function(err, rows){
                      if(err)
                        console.log(err);
                      else{

                        if (!fs.existsSync(dir+'/'+rows[0].id)) {
                          fs.mkdirSync(dir+'/'+rows[0].id);
                          console.log('motor current file created');
                        }
                        connection.query('CALL CreateLogTables(?)', rows[0].id, function(err, rows){
                          if(err)
                            console.log(err);
                          else{
                            console.log('tables created');
                            connection.commit(function(err) {
                              if (err) {
                                console.log(err);
                                connection.rollback(function() {
                                  throw err;
                                });
                              }
                              console.log('success!');
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
            //connection.commit();
            delete global.objCategoryList;
            delete global.objDeviceList;
            callback({status : "ok", message : ""});
          }
          else{
            callback({status : "error", message : objStatusIp.message});
          }
        });
      }
      else{
        callback({status : "error", message : objStatus.message});
      }
    });
  }

  function _UpdateDeviceInfo(deviceData, callback){
    var deviceNameList = [{"name" : deviceData.devName.trim()}];
    console.log(deviceNameList);
    validateDeviceNames(deviceNameList, function(objStatus){
      if(objStatus.isvalid || !deviceData.isValidateName){
        var deviceIPList = [{"ip" : deviceData.ip.trim()}];
        console.log(deviceIPList);
        validateDeviceIP(deviceIPList, function(objStatusIp){
          if(objStatusIp.isvalid || !deviceData.isValidateIp){
            var data = [deviceData.devName.trim(), deviceData.port.trim(), deviceData.ip.trim(), deviceData.devId];
            var sql = 'update tbl_devices set name = ? , port = ? , device_ip = ? where id = ?';
            connection.query(sql, data, function(err) {
              if (err) {
                return console.error(err.message);
              }
              else {
                delete global.objCategoryList;
                delete global.objDeviceList;
                _GetTreeData(function(error, data){
                  if(error)
                    console.log(error);
                  else
                    BuildObjectForTree(data, 'tree_data');
                })
                callback({status : "ok", message : ""});
              }
            });
          }
          else{
            callback({status : "error", message : objStatusIp.message});
          }
        });
      }
      else{
        callback({status : "error", message : objStatus.message});
      }
    });
  }

  function _SaveCategory(categoryName, callback){
    validateCategory(categoryName, function(objValidate){
      if(objValidate.isValid){
        connection.query('insert into tbl_devices_category (cat_name, created_on) values (?, CURRENT_TIMESTAMP);', categoryName, function(err){
          if(err){
            callback({status : "error", message : err});
          }
        })
        delete global.objCategoryList;
        delete global.objDeviceList;
        callback({status : "ok", message : ""});
      }
      else{
        callback({status : "error", message : objValidate.message});
      }
    })
  }

  function validateCategory(categoryName, callback){
    connection.query('select cat_name from tbl_devices_category where cat_name like ?', categoryName, function(err, rows) {
      if(err)
        callback({isValid:false, message: err});
      else if (rows.length > 0)
        callback({isValid:false, message: 'Category with name "'+ categoryName +'" already exist. Please choose a different name.'});
      else
        callback({isValid:true, message: ""});
    });
  }

  function validateDeviceNames(deviceList, callback){
    var where_clause = 'where name in ("'
    for (var i = 0; i < deviceList.length; i++) {
      where_clause += deviceList[i].name +'","'
    }
    where_clause = where_clause.slice(0, -2);
    where_clause += ');'
    var query = 'select name from tbl_devices ' + where_clause;
    connection.query(query, function(err, rows) {
      if(err)
        callback({isvalid:false, message: err});
      else if(rows.length > 0){
        var already_exist_device_name = "";
        for (var row in rows){
          already_exist_device_name += rows[row].name + ','
        }
        already_exist_device_name = already_exist_device_name.slice(0,-1);
        callback({isvalid:false, message: "Device(s) with following name(s) : " + already_exist_device_name + " already exist. Please select different name(s)."});
      }
      else{
        callback({isvalid:true, message: ""});
      }
    });
  }

  function validateDeviceIP(deviceList, callback){
    var where_clause = 'where device_ip in ("'
    for (var i = 0; i < deviceList.length; i++) {
      where_clause += deviceList[i].ip +'","'
    }
    where_clause = where_clause.slice(0, -2);
    where_clause += ');'
    var query = 'select device_ip from tbl_devices ' + where_clause;
    connection.query(query, function(err, rows) {
      if(err)
        callback({isvalid:false, message: err});
      else if(rows.length > 0){
        var already_exist_device_ip = "";
        for (var row in rows){
          already_exist_device_ip += rows[row].device_ip + ','
        }
        already_exist_device_ip = already_exist_device_ip.slice(0,-1);
        callback({isvalid:false, message: "Device(s) with following IP(s) : " + already_exist_device_ip + " already exist. Please select different IP(s)."});
      }
      else{
        callback({isvalid:true, message: ""});
      }
    });
  }

  function _GetDeviceInfo(deviceID, callback){
    connection.query("select name, device_location, software_version, hardware_version, CONCAT('http://', device_ip, ':', port) as ip, switch_direction, REPLACE(REPLACE(health_status,'G','Good'),'B','Bad') health_status, date_format(created_on, '%m/%d/%Y') as created_on, latch_out_status from tbl_devices where id = ?", deviceID, function(err, rows) {
      if(err){
        console.log(err);
      }
      callback(err, rows[0]);
    });
  }

  function _GetCategoryList(callback){
    connection.query('select id as cat_id, cat_name from tbl_devices_category order by cat_name;', function(err, rows) {
      callback(err, rows);
    });
  }

  function _GetSwopLogs(recordsPerPage, pageNumber, tableName, callback){
    var objData;
    var offset = recordsPerPage * (pageNumber - 1);
    offset = offset < 0 ? 0 : offset;
    connection.query('select count(*) Total from ' + tableName , function(err, rows) {
      if(err){
        objData = {status : "error", total_records : 0, swop_logs : ""};
        callback(objData);
      }
      else{
        var query = "SELECT id as rowid, time, REPLACE(REPLACE(start,'R','Reverse'),'N','Normal') start, REPLACE(REPLACE(end,'R','Reverse'),'N','Normal') end, opertime, mcamps, temp, throwcount from "+ tableName +" order by time desc LIMIT "+ recordsPerPage +" OFFSET "+ offset +";"
        connection.query(query, function(error, logs) {
          if(error){
            objData = {status : "error", total_records : 0, swop_logs : ""};
            console.log(error);
            callback(objData);
          }
          else{
            objData = {status : "ok", total : rows[0].Total, swop_logs : logs};
            callback(objData);
          }
        });
      }
    });
  }

  function _GetSnmpLogs(recordsPerPage, pageNumber, tableName, callback){
    var objData;
    var offset = recordsPerPage * (pageNumber - 1);
    offset = offset < 0 ? 0 : offset;
    connection.query('select count(*) Total from ' + tableName , function(err, rows) {
      if(err){
        objData = {status : "error", total_records : 0, snmp_logs : ""};
        callback(objData);
      }
      else{
        var query = "SELECT id as rowid, text_value, numeric_id, time, criticality from "+ tableName +" order by time desc LIMIT "+ recordsPerPage +" OFFSET "+ offset +";"
        connection.query(query, function(error, logs) {
          if(error){
            objData = {status : "error", total_records : 0, snmp_logs : ""};
            console.log(error);
            callback(objData);
          }
          else{
            objData = {status : "ok", total : rows[0].Total, snmp_logs : logs};
            callback(objData);
          }
        });
      }
    });
  }

  function _UpdateStatusField(fieldName, fieldValue, ip, numeric_id){
    if(fieldName == "health_status" || fieldName == "latch_out_status")
      var query = 'update tbl_devices set '+ fieldName +' = "'+ fieldValue +'", is_connected = "Y" where device_ip = "'+ ip +'";'
    else if (fieldName == 'is_connected')
      var query = 'update tbl_devices set '+ fieldName +' = "'+ fieldValue +'" where device_ip = "'+ ip +'";'
    else
      var query = 'update tbl_devices set '+ fieldName +' = '+ fieldValue +', is_connected = "Y" where device_ip = "'+ ip +'";'
    connection.query(query, function(err){
      if(err){
        console.log(err);
      }
      else{
        if(typeof global.objDeviceList !== 'undefined'){
          var objDevice = global.objDeviceList.find(x => x.ip === ip);
          if(typeof objDevice !== 'undefined'){

            //console.log('Previous Value = '+ objDevice[fieldName]);

            objDevice[fieldName] = fieldValue;
            objDevice['is_connected'] = fieldName == 'is_connected' ? fieldValue : 'Y';

            //console.log('New Value = '+ global.objDeviceList.find(x => x.ip === ip)[fieldName]);
          }
        }
        InsertSnmpTrapDetails(ip, '{"' + fieldName + '":"' + fieldValue + '"}', numeric_id, 'LOW');
        _GetTreeData(function(error, data){
          if(error)
            console.log(error);
          else
            BuildObjectForTree(data, 'refresh-colors');
        })
      }
    });
  }

  function InsertSnmpTrapDetails(ip, text_value, numeric_id, criticality){
    connection.query("select `VALUE` from config where `KEY` = 'SNMP_LOG_FLAG'", function(error,rows){
      if(error)
        console.log(error);
      else if(rows[0].VALUE == 'true'){
        connection.query("select id from tbl_devices where device_ip = ?", ip, function(err,data){
          if(err){
            console.log(err);
          }
          else{
            if(data.length > 0){
              var query = "insert into " + data[0].id + "_snmp (text_value, numeric_id, time, criticality) values ('" + text_value+ "','" + numeric_id + "', CURRENT_TIMESTAMP, '" + criticality +"');"
              connection.query(query, function(err){
                if(err){
                  console.log(err);
                }
              });
            }
          }
        });
      }
    });
  }

  function DecideColor(rawDeviceList, categoryList, emit_tag, callback){
    var deviceList=[];
    for (var i=0 ; i < rawDeviceList.length ; i++) {
      var objDeviceList = {};
      objDeviceList.dev_id = rawDeviceList[i].dev_id;
      objDeviceList.dev_name = rawDeviceList[i].dev_name;
      objDeviceList.cat_id = rawDeviceList[i].cat_id;
      objDeviceList.connection_status = rawDeviceList[i].is_connected == 'Y' ? 'green' : 'gray';
      objDeviceList.latch_out_status = rawDeviceList[i].latch_out_status == 'LO' ? 'red' : 'green';
      objDeviceList.health_status =  rawDeviceList[i].health_status ==  'G' ? 'green' : 'red';
      objDeviceList.aux_sensors_status = parseInt(rawDeviceList[i].aux_sensors_value) > parseInt(rawDeviceList[i].aux_sensors_threshold) ? 'yellow' : 'green';
      objDeviceList.motor_current_status = parseInt(rawDeviceList[i].motor_current_value) > parseInt(rawDeviceList[i].motor_current_threshold) ? 'yellow' : 'green';
      objDeviceList.motor_throw_time_status = parseInt(rawDeviceList[i].voltage_value) > parseInt(rawDeviceList[i].voltage_red_threshold) ? 'red' : (parseInt(rawDeviceList[i].voltage_value) > parseInt(rawDeviceList[i].voltage_yellow_threshold) ? 'yellow' : 'green');
      deviceList.push(objDeviceList);
    }
    callback(deviceList, categoryList, emit_tag);
  }

  function BuildObjectForTree(data, emit_tag){
    DecideColor(data.deviceList, data.categoryList, emit_tag, function(deviceList, categoryList, emit_tag){
      var treeData = categoryList;
      treeData.map(v => v.node = []);
      for (var device in deviceList){
        treeData.filter(x => x.cat_id === deviceList[device].cat_id).map(x => x.node.push(deviceList[device]));
      }
      io.emit(emit_tag, { status: '0',
        data : JSON.stringify(treeData)
      });
    })
  }

  function _updateBossConfig(param, callback){
    var flag = 0;
    for(var i=0; i < Object.keys(param).length; i++) {
    //console.log('updBossConf ----'+Object.keys(param)[i]);
    connection.query('update config set `VALUE` = ? where `KEY` = ?;', [ param[Object.keys(param)[i]], Object.keys(param)[i]], function(err,rows){
      flag ++;
      if(err){
        console.log(err);
        callback(err, {});
        return;
      }
      delete global.objCategoryList;
      delete global.objDeviceList;
      if(flag == Object.keys(param).length)
        callback(undefined, 'Records Update Successfully.');
    });
  }
}

function _GetCongigurationData(callback){
  connection.query('select * from config;', function(err, rows) {
    if(err)
      callback(err, '');
    else
      callback(undefined, rows);
  });
}

function _ValidateAndDeleteCategory(categoryID, callback){
  ValidateCategoryToDelete(categoryID, function(objStatus){
    if(objStatus.isvalid){
      connection.query("delete from tbl_devices_category where id = ?", categoryID, function(err, rows) {
        if(err)
          callback({status:"error", message: err});
        else{
          delete global.objCategoryList;
          delete global.objDeviceList;
          callback({status:"ok", message: "Category Deleted Successfully."});
        }
      });
    }
    else{
      callback({status:"validation_failed", message: objStatus.message});
    }
  })
}

function _DeleteDevice(deviceID, callback){
  connection.beginTransaction(function(err) {
    if (err) { throw err; }
    connection.query('delete from MOTOR_CURRENT_FILE where DEVICE_ID = ?', deviceID, function(instaDataDeleteError){
      if(instaDataDeleteError){
        console.log(instaDataDeleteError);
      }
      else{
        connection.query('delete from DEVICE_INST_DATA where device_id = ?', deviceID, function(instaDataDeleteError){
          if(instaDataDeleteError){
            console.log(instaDataDeleteError);
          }
          else{
            connection.query("delete from tbl_devices where id = ?", deviceID, function(err){
              if(err)
                console.log(err);
              else{
                var dropQuery = "DROP TABLE IF EXISTS " + deviceID + "_events, " + deviceID + "_maintenance, " + deviceID + "_snmp, " + deviceID + "_swop";
                connection.query(dropQuery, function(error){
                  if(error)
                    console.log(error);
                  else{
                    connection.commit(function(errInCommit) {
                      if (errInCommit) {
                        console.log(err);
                        connection.rollback(function() {
                          callback({status : "error", message : "error occurred while deleting device."})
                        });
                      }
                      else{
                        delete global.objCategoryList;
                        delete global.objDeviceList;
                        callback({status : "ok", message : "device deleted successfully."})
                      }
                    });
                  }
                });
              }
            })
          }
        });
      }
    });
  });
}

function ValidateCategoryToDelete(categoryID, callback){
  var query = 'select id from tbl_devices where category = "'+ categoryID +'"';
  connection.query(query, function(err, rows) {
    if(err)
      callback({isvalid:false, message: err});
    else if(rows.length > 0){
      callback({isvalid:false, message: "Category can not be deleted. Please move existing devices to another category before deleting."});
    }
    else{
      callback({isvalid:true, message: ""});
    }
  });
}

function _GetLogsSize(callback){
  var eventsQuery = "SELECT sum(round(((data_length + index_length) / 1024 / 1024), 2)) `combined_log_size` FROM information_schema.TABLES WHERE table_schema = 'ieccboss' AND table_name in (select CONCAT(id, '_events') as tableName from tbl_devices order by id asc);";
  var  maintenanceQuery = "SELECT sum(round(((data_length + index_length) / 1024 / 1024), 2)) `combined_log_size` FROM information_schema.TABLES WHERE table_schema = 'ieccboss' AND table_name in (select CONCAT(id, '_maintenance') as tableName from tbl_devices order by id asc);";
  var  snmpQuery = "SELECT sum(round(((data_length + index_length) / 1024 / 1024), 2)) `combined_log_size` FROM information_schema.TABLES WHERE table_schema = 'ieccboss' AND table_name in (select CONCAT(id, '_snmp') as tableName from tbl_devices order by id asc);";
  var  swopQuery = "SELECT sum(round(((data_length + index_length) / 1024 / 1024), 2)) `combined_log_size` FROM information_schema.TABLES WHERE table_schema = 'ieccboss' AND table_name in (select CONCAT(id, '_swop') as tableName from tbl_devices order by id asc);";
  connection.query("select `VALUE` from config where `KEY` = 'LOG_THRESHOLD';", function(err, rows) {
    if(err)
      console.log(err);
    else{
      connection.query(eventsQuery, function(error, rowsData) {
        if(err)
          console.log(err);
        else{
          var objLog = { logsize : rowsData[0].combined_log_size, thershold : rows[0].VALUE, log_name : 'events' };
          io.emit('logStorageInfo', objLog);
        }
      });
      connection.query(maintenanceQuery, function(error, rowsData) {
        if(err)
          console.log(err);
        else{
          var objLog = { logsize : rowsData[0].combined_log_size, thershold : rows[0].VALUE, log_name : 'maintenance' };
          io.emit('logStorageInfo', objLog);
        }
      });
      connection.query(snmpQuery, function(error, rowsData) {
        if(err)
          console.log(err);
        else{
          var objLog = { logsize : rowsData[0].combined_log_size, thershold : rows[0].VALUE, log_name : 'snmp' };
          io.emit('logStorageInfo', objLog);
        }

      });
      connection.query(swopQuery, function(error, rowsData) {
        if(err)
          console.log(err);
        else{
          var objLog = { logsize : rowsData[0].combined_log_size, thershold : rows[0].VALUE, log_name : 'switch_operations' };
          io.emit('logStorageInfo', objLog);
        }

      });
      callback(rows[0].VALUE);
    }
  });
}

function _GetMinAndMaxDate(logName, callback){
  connection.query('select id from tbl_devices order by id asc',function(err, rows){
    var query;
    if(logName == "events"){
      query = 'SELECT MIN(time) as minDate, MAX(time) as maxDate FROM ( SELECT time FROM ' + rows[0].id + '_events ';
      for(var i=1; i<rows.length; i++){
        query += 'UNION SELECT time FROM ' + rows[i].id + '_events ';
      }
      query += ') AS events;';
      connection.query(query, function(err, data){
        if(err)
          console.log(err);
        else if(data.length > 0){
          callback({minDate : data[0].minDate, maxDate : data[0].maxDate});
        }
      })
    }
    else if(logName == "swop"){
      query = 'SELECT MIN(time) as minDate, MAX(time) as maxDate FROM ( SELECT time FROM ' + rows[0].id + '_swop ';
      for(var i=1; i<rows.length; i++){
        query += 'UNION SELECT time FROM ' + rows[i].id + '_swop ';
      }
      query += ') AS swop;';
      connection.query(query, function(err, data){
        if(err)
          console.log(err);
        else if(data.length > 0){
          callback({minDate : data[0].minDate, maxDate : data[0].maxDate});
        }
      })
    }
    else if(logName == "maintenance"){
      query = 'SELECT MIN(LOG_DATE) as minDate, MAX(LOG_DATE) as maxDate FROM ( SELECT LOG_DATE FROM ' + rows[0].id + '_maintenance ';
      for(var i=1; i<rows.length; i++){
        query += 'UNION SELECT LOG_DATE FROM ' + rows[i].id + '_maintenance ';
      }
      query += ') AS maintenance;';
      connection.query(query, function(err, data){
        if(err)
          console.log(err);
        else if(data.length > 0){
          callback({minDate : data[0].minDate, maxDate : data[0].maxDate});
        }
      })
    }
    else if(logName == "snmp"){
      query = 'SELECT MIN(time) as minDate, MAX(time) as maxDate FROM ( SELECT time FROM ' + rows[0].id + '_snmp ';
      for(var i=1; i<rows.length; i++){
        query += 'UNION SELECT time FROM ' + rows[i].id + '_snmp ';
      }
      query += ') AS maintenance;';
      connection.query(query, function(err, data){
        if(err)
          console.log(err);
        else if(data.length > 0){
          callback({minDate : data[0].minDate, maxDate : data[0].maxDate});
        }
      })
    }
    else if(logName == "motor_current"){
      query = 'select MIN(TIME_STAMP) as minDate, MAX(TIME_STAMP) as maxDate from MOTOR_CURRENT_FILE;'
      connection.query(query, function(err, data){
        if(err)
          console.log(err);
        else if(data.length > 0){
          callback({minDate : data[0].minDate, maxDate : data[0].maxDate});
        }
      })
    }
  })
}

function _DeleteLogs(param, callback){
  connection.query('select id from tbl_devices order by id asc',function(err, rows){
    var query = "";
    if (param.logName == "events"){
      for(var i=0; i< rows.length; i++){
        query += 'delete from ' + rows[i].id + '_events where time between "' + param.minDate + '" and "' + param.maxDate + ' 23:59:59";'
      }
    }
    else if(param.logName == "swop"){
      for(var i=0; i< rows.length; i++){
        query += 'delete from ' + rows[i].id + '_swop where time between "' + param.minDate + '" and "' + param.maxDate + ' 23:59:59";'
      }
    }
    else if(param.logName == "maintenance"){
      for(var i=0; i< rows.length; i++){
        query += 'delete from ' + rows[i].id + '_maintenance where LOG_DATE between "' + param.minDate + '" and "' + param.maxDate + ' 23:59:59";'
      }
    }
    else if(param.logName == "snmp"){
      for(var i=0; i< rows.length; i++){
        query += 'delete from ' + rows[i].id + '_snmp where time between "' + param.minDate + '" and "' + param.maxDate + ' 23:59:59";'
      }
    }
    console.log(query);
    multipleQueryConnection.query(query, function(error){
      if(error){
        console.log(error);
        callback({status : 'error'})
      }
      else
        callback({status : 'ok'})
    });
  });
}

function _GetMotorCurrentFileLocationForDelete(param, callback){
  var query = 'select CONCAT(DEVICE_ID, "/", FILE_NAME, ".json") as File_Location from MOTOR_CURRENT_FILE where TIME_STAMP between "' + param.minDate + '" and "' + param.maxDate + ' 23:59:59";';
  console.log(query);
  connection.query(query,function(err, rows){
    if(err)
      console.log(err);
    else
      callback(rows);
  });
}

function _DeleteMotorCurrentFiles(param, callback){
  var query = 'delete from MOTOR_CURRENT_FILE where TIME_STAMP between "' + param.minDate + '" and "' + param.maxDate + ' 23:59:59";';
  console.log(query);
  connection.query(query, function(err){
    if(err){
      console.log(err);
      callback({status : 'error'});
    }
    else
      callback({status : 'ok'});
  });
}

function _GetConfigurableIP(callback){
 connection.query("select `VALUE` from config where `KEY` = 'BOSS_IP_ADDRESS';", function(err, data){
  if(err){
    console.log(err);
  }
  else{
    callback(data);
  }
});
}

var objOperations = {
  GetTreeData : _GetTreeData,
  UpdateDeviceCategory : _UpdateDeviceCategory,
  AddNewDevices : _AddNewDevices,
  GetDeviceInfo : _GetDeviceInfo,
  GetCategoryList : _GetCategoryList,
  SaveCategory : _SaveCategory,
  GetSwopLogs : _GetSwopLogs,
  GetSnmpLogs : _GetSnmpLogs,
  UpdateStatusField : _UpdateStatusField,
  updateBossConfig : _updateBossConfig,
  GetCongigurationData : _GetCongigurationData,
  ValidateAndDeleteCategory : _ValidateAndDeleteCategory,
  UpdateDeviceInfo : _UpdateDeviceInfo,
  GetLogsSize : _GetLogsSize,
  GetMinAndMaxDate : _GetMinAndMaxDate,
  DeleteLogs : _DeleteLogs,
  GetMotorCurrentFileLocationForDelete : _GetMotorCurrentFileLocationForDelete,
  DeleteMotorCurrentFiles : _DeleteMotorCurrentFiles,
  GetConfigurableIP : _GetConfigurableIP,
  DeleteDevice : _DeleteDevice
}

return objOperations;
}
