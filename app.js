
DIRECTORY=/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin
DIRECTORY_NW=/usr/share/iecc-asts/app/node_modules/nw
DIRECTORY_NW_GYP=/usr/share/iecc-asts/app/node_modules/nw-gyp
DIRECTORY_EXP=/usr/share/iecc-asts/app/node_modules/express
DIRECTORY_EJS=/usr/share/iecc-asts/app/node_modules/ejs
DIRECTORY_EXP_SES=/usr/share/iecc-asts/app/node_modules/express-session
DIRECTORY_COC_PAR=/usr/share/iecc-asts/app/node_modules/cookie-parser
DIRECTORY_BOD_PAR=/usr/share/iecc-asts/app/node_modules/body-parser
DIRECTORY_SW_AlER=/usr/share/iecc-asts/app/node_modules/sweetalert2
DIRECTORY_SK_IO=/usr/share/iecc-asts/app/node_modules/socket.io
DIRECTORY_BUNYAN=/usr/share/iecc-asts/app/node_modules/bunyan
DIRECTORY_SNMP=/usr/share/iecc-asts/app/node_modules/snmpjs
DIRECTORY_CRON=/usr/share/iecc-asts/app/node_modules/node-cron
DIRECTORY_REST_CLIENT=/usr/share/iecc-asts/app/node_modules/node-rest-client	
DIRECTORY_REPO=/usr/share/iecc-asts/app/repo
INS_INTRPT=/usr/share/iecc-asts/app/intrupted.txt
DB_CREATED=/usr/share/iecc-asts/app/dbCreated.txt
userchoice="$@"
if [ "${userchoice}" == "install" ]
then
if [[ -f db_connection.json ]]
then
cp db_connection.json /usr/share/iecc-asts/app/
cd /usr/share/iecc-asts/app/
if [ -d "${DIRECTORY_REPO}" ]
then
rm -rf /usr/share/iecc-asts/app/repo
mkdir /usr/share/iecc-asts/app/repo
fi
if [ ! -d "${DIRECTORY}" ]
then
wget http://nodejs.org/dist/v7.6.0/node-v7.6.0-linux-x64.tar.gz
tar xvfz node-v7.6.0-linux-x64.tar.gz
fi
export PATH=$PATH:/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin:/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/node
if [ ! -d "${DIRECTORY_NW_GYP}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install nw-gyp;
fi
if [ ! -d "${DIRECTORY_NW}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install nw@0.12.3;
fi

markdownVersion="$(npm list markdown 2>&1)"
if [[ ! "${markdownVersion}" =~ "markdown@0.5.0" ]]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install markdown;
fi
if [ ! -d "${DIRECTORY_EXP}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install express;
fi
if [ ! -d "${DIRECTORY_EJS}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install ejs;
fi
if [ ! -d "${DIRECTORY_EXP_SES}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install express-session;
fi
if [ ! -d "${DIRECTORY_COC_PAR}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install cookie-parser
fi
if [ ! -d "${DIRECTORY_BOD_PAR}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install body-parser
fi
if [ ! -d "${DIRECTORY_SW_AlER}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install sweetalert2
fi
if [ ! -d "${DIRECTORY_SK_IO}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install socket.io
fi
if [ ! -d "${DIRECTORY_BUNYAN}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install bunyan
fi
if [ ! -d "${DIRECTORY_SNMP}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install snmpjs
fi
if [ ! -d "${DIRECTORY_CRON}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install node-cron
fi
if [ ! -d "${DIRECTORY_REST_CLIENT}" ]
then
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install node-rest-client
fi

/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install mysql
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install node-fetch
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install ip
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install console-stamp
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install net-snmp
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install get-folder-size@1.0.0
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/npm install nodejs-disks

/usr/share/iecc-asts/app/node_modules/nw/bin/nw .;
if [[ -f "${INS_INTRPT}" ]]
then
rm -rf /usr/share/iecc-asts/app/intrupted.txt;
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/node server.js;
fi
else
echo "No db_connection.json file found, create db_connection.json in current directory and try again";
fi
elif [ "${userchoice}" == "start" ]
then
if [[ -f "${DB_CREATED}" ]]
then
echo "starting ASTS server....";
/usr/share/iecc-asts/app/node-v7.6.0-linux-x64/bin/node /usr/share/iecc-asts/app/server.js;
else
echo "Invalid request first run command iecc-asts install to install server";
fi
elif [ "${userchoice}" == "stop" ]
then
echo "stopping IECC-ASTS server....";
pkill node;
else
echo "Invalid request, run command iecc-asts install to install server,iecc-asts start to start server, iecc-asts stop to stop server.";
fi
