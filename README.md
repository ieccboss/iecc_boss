Pre-requisites :
################################################################################################

1. Ubuntu 16.04, 64 bit machine is required, these packages can work only on this environment.
2. NodeJs must be installed on the system.
3. Machine must be connected to internet during installation
4. mysql must be installed on this machine
5. db_connection.json file must be present in the same directory, from where iecc-boss install is run

IECC-BOSS package Installation steps:
################################################################################################
1. Download the package
2. sudo dpkg -i iecc-bossXXX(complete location if not in same directory)
3. create db_connection.json file with mysql details in proper format, for example :
{
	"host": "localhost",
	"user": "root",
	"password": "yourpassword",
	"database": "ieccboss",
	"port" : "3306"
}
4. sudo iecc-boss install
5. Wait for the installation of required packages to complete
6. If you see error for some reason, run sudo iecc-boss install command again
7. License disclaimer will appear
8. Follow the onscreen instructions.
9. IeccBoss database will be created and server will be started, the url of iecc-boss app will appear on the console
10. Hit the url to view iecc-boss app in browser
11. sudo iecc-boss stop, to stop the server
12. sudo iecc-boss start, to stop the server. Again the url of iecc-boss app will appear on the console